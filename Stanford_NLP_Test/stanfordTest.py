# -*- coding: utf-8 -*-

import nltk
from nltk.tag import StanfordNERTagger
from nltk.tag import StanfordPOSTagger
from nltk.tokenize import word_tokenize
from nltk.tokenize import StanfordTokenizer
from nltk import ne_chunk
import nltk.data
import os

#required java installation
os.environ['JAVAHOME'] = "C:/Program Files/Java/jdk1.8.0_05/bin"
#sentence tokenizer
sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
#stanford tokenizer to comply with stanford taggers
tokenizer = StanfordTokenizer('./stanford-postagger/stanford-postagger.jar')
st = StanfordNERTagger('./stanford_ner/classifiers/english.muc.7class.distsim.crf.ser.gz',
					   './stanford_ner/stanford-ner.jar',
					   encoding='utf-8')
sp = StanfordPOSTagger('./stanford-postagger/models/english-bidirectional-distsim.tagger',
					   './stanford-postagger/stanford-postagger.jar',
					   encoding='utf-8')

#text = """ Charlotte Hornets, Los Angeles Lakers, Golden State Warriors. Hello there the name of the country is Taiwan. I am testing this when not in the United States. Paris is not a city that I want to live in. Therefore Starbucks needs to be born in 1949."""

"""
#(28 January 1921 – 29 January 2014)
#(born in 1922)
#(born Hilal Tuba Tosun in 1970)
#(born December 31, 1986)
#(Greek: Χρήστος Σαλούστρος; born March 29, 1990)
#(pronounced "bee-line"; born February 5, 1953)
#(1919 to 18 July 1994)
#(though his name is Americanized as Zen Puzinauskas; born March 4, 1920; died July 16, 1995)
#(born in 1917, Beloretsk, Bashkortostan - 2010 in USA)
#(born 11 May 1962 in Sofia)
(September 9, 1907 – November 9, 1985)
(1913 – 1981)
(born in 1916)
(Americanized his name as Connie Savickas; October 14, 1908, Punsk – 1992, Chicago)
(born c. 1939)
"""

'''
Ball_text = """
Jeremy Shu-how Lin (born August 23, 1988) is an American professional basketball player for the Brooklyn Nets of the National Basketball Association (NBA)."""
'''
'''Ball_text = """Nazr Tahiru Mohammed (pronounced NAH-zee; born September 5, 1977) is an American professional basketball player for the Oklahoma City Thunder of the National Basketball Association (NBA). He played college basketball for Kentucky.
The son of an immigrant from Ghana, Mohammed was raised in Chicago and attended high school at Kenwood Academy, graduating in 1995."""
'''
'''Ball_text = """Henry G. "Dutch" Dehnert (April 5, 1898 – April 20, 1979) was an American basketball player whose career lasted from 1915 to 1935.
Dehnert, a bulky forward born in New York City, New York, is mostly known for his time with the Original Celtics and is sometimes credited with inventing the pivot play. He later coached several teams in the NBL, ABL and BAA.
One of those teams Dehnert coached was the Sheboygan Red Skins, who won NBL divisional titles in 1944-45 and 1945-46 under Dehnert's guidance. Dehnert's greatest coup during his time in Sheboygan was his signing of three East Coast stars: Al Lucas of Fordham, Al Moschetti of St. John's and Bobby Holm of Seton Hall. Buoyed by this added strength, the Red Skins took a 2-0 lead over the Fort Wayne Zollner Pistons in the 1945 NBL championship series, only to be swept in the remaining three games. In 1946, Dehnert led Sheboygan to a meeting with the vaunted Rochester Royals in the championship series. Rochester swept the Red Skins. The next season, Dehnert became first head coach of the Cleveland Rebels for the Basketball Association of America's first season.
He was the uncle of Providence Steamrollers player Red Dehnert. He played for the Boston Celtics. He is part of the Los Angeles Lakers
"""
'''

Ball_text = """Kobe Bean Bryant (born August 23, 1978) is an American retired professional basketball player. He played his entire 20-year career with the Los Angeles Lakers of the National Basketball Association (NBA). He entered the NBA directly from high school and won five NBA championships with the Lakers. Bryant is an 18-time All-Star, 15-time member of the All-NBA Team, and 12-time member of the All-Defensive team."""



chunk = r"""Chunk: }<\(>|<\)>{
{<\'\'>|<``>}
{<VB.?>+<PRP.?>*<DT.?>*<JJ.?>*<RB.?>*<VB.?>*<IN.?>*<PRP\$>*<NN.?>*<IN.?>*<DT.?>*<NNP.?>+}
{<VB.?>+<.>*<NNP.?>+}
<``>{}<NNP.?>+
<NNP.?>+{}<\'\'>
<\'\'>{}<NNP.?>+
<NNP.?>+{}<``>
"""
chunkParser = nltk.RegexpParser(chunk)
sentToken = sent_detector.tokenize(Ball_text)
#for sent in sentToken:
tokenized_text = tokenizer.tokenize(Ball_text)
POS_text = sp.tag(tokenized_text)
chunked = chunkParser.parse(POS_text)
#print chunked

results = []
for chunk in chunked.subtrees(filter = lambda x: x.label() == 'Chunk'):
    results.append(" ".join([a for (a,b) in chunk.leaves()]))
    #results.append(' '.join([a for (a,b) in chunk.leaves()]))
       
    #chunked.draw()

print results
#test_text = regexp_tagger.tag(tokenized_text)
#nltk_text = nltk.pos_tag(tokenized_text)
#classified_text = st.tag(tokenized_text)
#tagged_text = nltk.pos_tag(tokenized_text)
#NLTKtag = nltk.ne_chunk(tagged_text, binary = True)




#{<VB.?>*<IN.?>*<DT.?>*<NNP.?>+}



#print chunked
#print(classified_text)
#print('\n')
#print(test_text)
#print nltk_text
#print(NLTKtag)
