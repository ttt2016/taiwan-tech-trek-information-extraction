from nltk.tag.stanford import StanfordNERTagger
from nltk.tokenize import word_tokenize
from nltk import  ne_chunk
import os

st = StanfordNERTagger("../stanford-ner/classifiers/english.all.3class.distsim.crf.ser.gz",
               "../stanford-ner/stanford-ner.jar")
print st.tag("Rami Eid is studying at Stony Brook University in NY".split())
print (help(st.tag_sents))

