'''
Created on Aug 1, 2016

@author: Janice
'''
from __future__ import print_function
# from __future__ import division, unicode_literals 
import codecs
from bs4 import BeautifulSoup
import re


def get_code_nation(file_name):
    pat = re.compile(r"((?:[A-Z]{1}[a-z]+)(?:[ -][A-Z]{1}[a-z]+)?(?:[ -][A-Z]{1}[a-z]+)?)")
    rm_pat = re.compile(r"[,\(].*")
    f=codecs.open(file_name, 'r', 'utf-8')
    document= BeautifulSoup(f.read())
    d = dict()
    iso_table = document.findAll('table')[2]
    inside = False
    iso = iso_table.findAll('tr')[2:]
    for i in iso:
        cur = i.findAll('td')
        if cur[1].string == None:
            text = cur[1].get_text()
            m = re.match(pat, text)
            country = m.group(0)
        else:
            country = cur[1].string.strip()
        
        country = re.sub(rm_pat, '', country)

        try:
            d[country] = cur[3].string.strip()
        except:
            print("error")
    return d
        

if __name__ == "__main__":
    file_name = "iso_to_nationality.html"
    d = get_code_nation(file_name)
    for i in sorted(d):
        print(u"{:30}  {}".format(i, d[i]))
