'''
Created on Jul 29, 2016

@author: Janice
'''


import re
import nltk
from lxml import etree as ET
from nltk.tag.stanford import StanfordPOSTagger
from nltk.stem import PorterStemmer
from nltk.tokenize import StanfordTokenizer
from collections import defaultdict

# def extract_nats(text, sp, tokenizer):
#     nnp = []
#     not_nnp = []
#     for i in text[:60]:
#         tok = tokenizer.tokenize(i)
#         pos = sp.tag(tok)
#         tnnp = []
#         for x,y in pos: 
#         #             print(tnnp)
#             if y in [u"NNP", u"NNPS",u"FW"]:
#                 #tnnp.append(str(x.encode('utf8')))
#                 tnnp.append(x)
#                 print("AAAA")
#             else:
#                 #not_nnp.append(str(x.encode('utf8')))
#                 not_nnp.append(x)
#             nnp.extend(tnnp)
#     return nnp
# #     text = re.sub(r"\|", " ", text)
# #     text = re.sub(r"\(|\)", "", text)
# 
# #     for i in text:
# #         print(sp.tag(str(i.encode('utf8'))))
def stem_nat(word):
    if word[-1] == u's' and word[-2] != u's':
        return word[:-1]
    return word

if __name__ == "__main__":
    sp = StanfordPOSTagger('../stanford-postagger/models/english-bidirectional-distsim.tagger',
                       '../stanford-postagger/stanford-postagger.jar')#, encoding = 'utf-8')
    tokenizer = StanfordTokenizer('../stanford-postagger/stanford-postagger.jar')#, encoding='utf-8')
    ps = PorterStemmer()
    
    file_name = "extract_natList.xml"
    tree = ET.iterparse(file_name, events=('end',), tag='doc', encoding='utf8')
    pat = re.compile(r"\*{0,3} ?\[\[.*?(?:Category:)?(?:List of |Lists of |)?(.*?)(?:\|(.*?))?\]\]")
    for event, page in tree: 
        text = re.findall(pat, page.text)
        page.clear()

    rm_pat = re.compile(r"people|(nationality)|\(.*?\)|(Category)|:|(Lists of )|speakers|groups|citizens")
    single_words = []
    mult_words = []
    for a,b in text:
        a = re.sub(rm_pat, "", a).strip()
        b = re.sub(rm_pat, "", b).strip()
        la = a.split()
        lb = b.split()
        if len(la) == 1:
            if a not in single_words and len(a) > 2:
                if a[0] in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
                    single_words.append(a)
        else:
            if a not in mult_words:
                mult_words.append(a)
        if len(lb) == 1:
            if lb not in single_words and len(b) > 2:
                if a[0] in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
                    single_words.append(b)
        else:
            if b not in mult_words:
                mult_words.append(b)
    single_words = sorted(set(single_words))
#     print("Single")
#     for i in range(0,len(single_words), 20):
#         print(single_words[i:min(i+20,len(single_words))-1])
#     print("Multi")
#     for i in range(0,len(mult_words), 20):
#         print(mult_words[i:min(i+20,len(mult_words))-1])
    
    tag_types = defaultdict(set)

    keep_mult_words = []
    
    valid_tags = [u"NNP NNP CC NNP", u"NNP NNPS", u"NNP NNS", u"NNP NNP",
                  u"NNP NNP NNPS", u"JJ NNPS", u"JJ NNS", u"JJ NNP"]
    for words in mult_words:
        tok = tokenizer.tokenize(words)
        tags = sp.tag(tok)
        if " ".join([t[1] for t in tags]) in valid_tags:
            if words[0] in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
                keep_mult_words.append(words)

 
    countries = single_words + keep_mult_words

    
    wf = open("nat_db2.txt","w")
    wf2 = open("nat_db2stem.txt","w")
    for i in countries:
        wf.write(u"{}\n".format(stem_nat(i).encode('ascii','ignore')))
    for i in countries:
        wf2.write(u"{:20}  {}\n".format(stem_nat(i).encode('ascii', 'ignore'), i.encode('ascii', 'ignore')))
    wf.close()
    wf2.close()
    print("End")

