'''
Created on Jul 27, 2016

@author: Janice
'''
import re
import nltk
from lxml import etree as ET
from nltk.tag.stanford import StanfordPOSTagger
from nltk.stem import PorterStemmer
from nltk.tokenize import StanfordTokenizer

# >>> from collections import OrderedDict
# >>> list(OrderedDict.fromkeys('abracadabra'))
# ['a', 'b', 'r', 'c', 'd']

#ps = PorterStemmer()
#words = word_tokenize(new_text)
#    print(ps.stem(w))

def extract_nats(text, sp, tokenizer):
    nnp = []
    not_nnp = []
    for i in text[:60]:
        tok = tokenizer.tokenize(i)
        pos = sp.tag(tok)
        tnnp = []
        for x,y in pos: 
        #             print(tnnp)
            if y in ["NNP", "NNPS","FW"]:
                tnnp.append(str(x.encode('utf8')))
            else:
                not_nnp.append(str(x.encode('utf8')))
            nnp.extend(tnnp)
    return nnp
#     text = re.sub(r"\|", " ", text)
#     text = re.sub(r"\(|\)", "", text)

#     for i in text:
#         print(sp.tag(str(i.encode('utf8'))))

if __name__ == "__main__":
    sp = StanfordPOSTagger('../stanford-postagger/models/english-bidirectional-distsim.tagger',
                       '../stanford-postagger/stanford-postagger.jar')#, encoding = 'utf-8')
    word_tokenize = StanfordTokenizer('../stanford-postagger/stanford-postagger.jar')#, encoding='utf-8')
    ps = PorterStemmer()
    
    file_name = "extract_natList.xml"
    tree = ET.iterparse(file_name, events=('end',), tag='doc', encoding='utf8')
    pat = re.compile(r"\*{0,3} ?\[\[.*?(?:Category:)?(?:List of |Lists of |)?(.*?)\]\]")
    for event, page in tree: 
        m = re.findall(pat, page.text)
        list_of_nat = extract_nats(m, sp, word_tokenize)
        page.clear()
    wf = open("nat_db2.txt","w")
    for i in list_of_nat:
        wf.write("{}\n".format(ps.stem(str(i).decode('utf8'))))
    wf.close()
    print("End")

