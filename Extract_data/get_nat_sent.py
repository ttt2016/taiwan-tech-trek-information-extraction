'''
Created on Aug 3, 2016

@author: Janice & Andrew
'''

from __future__ import print_function
import os
from lxml import etree as ET
import re
from collections import defaultdict



def get_infobox_vals(tree):
    #    code_to_nationality, countrySet, codeSet = decode_nat()
    pat = re.compile(r"(<.*>)|({{citation.*)")
    nat = set()
    nat_symbol = set()
    subpat = re.compile(r"\(.*?\)|\-|\|")
    for event, page in tree:
        if page.find('info') != None:
            info = page.find('info')
            for line in info.text.split("\n"):
                if u"nationality" in line.lower():
                    line = re.sub(pat, '', line)
                    l = line.split("=")
                    if l[0].strip() != u"nationality":
                        continue
                    l = filter(None, l)
                    if len(l) > 2:
                        print("len>2:", l)
                    elif len(l) == 2:
                        for n in l:
                            if len(n.strip()) == 0:
                                break
                            nationality = [n.strip() for n in l[1].split("/")]
                            for nation in nationality:
                                if "[[" in nation or "{{" in nation:
                                    nat_symbol.add(nation)
                                elif re.search(subpat,nation):
                                    m = re.sub(subpat, ",", nation)
                                    for n in m.split(","):
                                        if len(n.strip())>0 and n.strip()[0].isupper():
                                            nat.add(n.strip())
                                elif "," in nation:
                                    for n in nation.split(","):
                                        if len(n.strip())>0 and n.strip()[0].isupper():
                                            nat.add(n.strip())
                                else:
                                    nat.add(nation)
                    break
        page.clear()
    return (nat,nat_symbol)
            
def match(d, line, *args): #m1, m2, m3):
    for m in args:
        if m[0]:
            d[m[1]].append(line)
            return True
    return False

def extract_symbol_phrase(lines): #lines is a list
    #c - code        N - nationality
    #C - Country     CN - [country_to_]nationality
    #xy (either)
    #x_y (separate)
    #    {{flagicon|INA}} Indonesian
    patflagCc_N = re.compile(r"^\{\{flagicon\|.+?\}\}[ ]?([A-Z][A-Za-z/-]+)$")
    #        American {{flagicon|USA}}
    patN_Cc = re.compile(r"^([A-Z][A-Za-z/-]+)[ ]?\{\{flagicon\|.+?\}\}$")
    #    [[Icelanders|Icelandic]]
    patC_N = re.compile(r'^\[\[([A-Za-z\- ]*?(\([A-Za-z\- ]+\))?\|([A-Za-z\- ]*?))\]\]$')

    #    [[Croatia]]n
    patCN = re.compile(r"^((?:\[\[[A-Za-z \-]*?\]\])(?:[a-z]+))$")
    #    {{flag|Lithuania}}n
    patflagCN = re.compile(r'^((?:\{\{flag\|[[A-Za-z \-]*?\}\})(?:[a-z]+))$')
    #    {{flagicon|Jordan}} [[Jordan]]ian
    patflagC_CN = re.compile(r'^\{\{flagicon\|([A-Za-z\- ]+?)\}\}[ ]{0,3}\[\[[A-Z][A-Za-z\- ]+\]\][a-z]+$')
    
    #    {{FRA}}-{{BEN}}
    patc = re.compile(r'(\{\{[A-Z]{2,3}\}\}){1,3}')
    #    [[Taiwan]]
    patC = re.compile(r'^\[\[([A-Za-z\- ]*?)\]\]$')
    #    {{flag|Canada}}
    patflagC = re.compile(r'^\{\{flag\|([A-Za-z -]+?)\}\}$')
    #    {{flagicon|Philippines}} [[Philippines]]
    patflagC_C = re.compile(r'(\{\{flagicon\|([A-Za-z\- ]+?)\}\})[ ]{0,3}(\[\[[A-Z][A-Za-z\- ]+\]\])$')    
    #    {{flagicon|USA}} [[United States|USA]]
    patflagc_C_cn = re.compile(r'^\{\{flagicon\|([A-Za-z\- ]+?)\}\}[ ]{0,3}\[\[([A-Za-z\- ]*?(\([A-Za-z\- ]+\))?\|([A-Za-z\- ]*?))\]\]$')
    
    patfinder = re.compile(r"(\[\[[A-Za-z' \-]+\]\](?![a-z]))|(\[\[[A-Za-z' \-]+\]\][a-z]+)|(\[\[[A-Za-z' \-]+\|[A-Za-z' \-]+\]\](?![a-z]))|(\[\[[A-Za-z' \-]+\|[A-Za-z' \-]+\]\][a-z]+)")
    #A: [[Czechoslovak]] (patC)
    #B: [[Jordan]]ian (patCN)
    #C: [[French people|French]] (patC_N)
    #D: [[Republic of the Congo|Congo]]lese (patC_CN)
    
    d = defaultdict(list)
    for i in lines:
        m1 = re.match(patflagCc_N,i)
        m2 = re.match(patN_Cc,i)
        m3 = re.match(patC_N,i)
        if match(d, i, (m1,'flagCc_N'),(m2,'N_Cc'), (m3,'C_N')):
            continue  
        m4 = re.match(patCN,i)
        m5 = re.match(patflagCN, i)
        m6 = re.match(patflagC_CN,i)
        if match(d, i, (m4, 'CN'), (m5,'flagCN'),(m6, 'flagC_CN')):
            continue
        m8 = re.match(patC,i)
        m9 = re.match(patflagC, i)
        m10 = re.match(patflagC_C, i)
        m11 = re.match(patflagc_C_cn, i)
        if match(d, i, (m8, 'C'), (m9, 'flagC'), (m10, 'flagC_C'), (m11, 'flagc_C_cn')):
            continue
        m7list = re.findall(patc, i)
        if len(m7list) > 0:
            for i in m7list:
                d['c'].append(i)            
            continue
        m_findlist = re.findall(patfinder, i)
        if len(m_findlist) > 0:
            for find in m_findlist:
                if find[0]:
                    d['C'].append(i)
                elif find[1]:
                    d['CN'].append(i)
                elif find[2]:
                    d['C_N'].append(i)
                elif find[3]:
                    d['C_CN'].append(i)
                else:
                    print("Error: {}".format(i))
            continue
        print("No match error: {}".format(i))
    return d

if __name__ == "__main__":
    filename = r"..\bball_finalcombined.xml"
    tree = ET.iterparse(filename,events=('end',), tag='doc')

    natSet, nat_symbol = get_infobox_vals(tree)
    symbol_dict = extract_symbol_phrase(list(nat_symbol))
    for a,b in symbol_dict.items():
        print(a)
        for i in b[-3:]:
            print("  {}".format(i))
            
"""
c
  {{USA}}
  {{TUR}}
  {{CAN}}
flagC_CN
  {{flagicon|Estonia}} [[Estonia]]n
  {{flagicon|Israel}} [[Israel]]i
  {{flagicon|EST}} [[Estonia]]n
flagC
  {{flag|Canada}}
  {{flag|Philippines}}
  {{flag|Germany}}
CN
  [[Iceland]]ic
  [[Estonia]]n
  [[Uruguay]]an
N_Cc
  American {{flagicon|USA}}
C_CN
  [[Republic of the Congo|Congo]]lese
  [[FYR Macedonia|Macedonia]]n
flagCN
  {{flag|Lithuania}}n
C
  [[Tunisia]]
  [[United Arab Emirates]]
  [[Qatar]]
C_N
  [[Bulgaria|Bulgarian]]
  [[France|French]]
  [[Azerbaijanis|Azerbaijani]]
flagC_C
  {{flagicon|Philippines}} [[Philippines]]
flagc_C_cn
  {{flagicon|CAN}} [[Canada|Canadian]]
  {{flagicon|Greece}} [[Greece|Greek]]
  {{flagicon|United States}} [[United States|American]]
flagCc_N
  {{flagicon|KOR}}Korean
  {{flagicon|USA}} American
  {{flagicon|PHI}} Filipino
"""
    

    

