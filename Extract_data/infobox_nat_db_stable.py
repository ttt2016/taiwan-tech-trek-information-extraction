'''
Created on Jul 29, 2016

@author: Janice
'''

from __future__ import print_function
import os
from lxml import etree as ET
import re
from collections import defaultdict

import country_to_nationality as c_nat
import code_to_country as iso_c
import numpy

from inspect import currentframe, getframeinfo #debug print line number
from scipy.optimize import linesearch
cf = currentframe()
filename = getframeinfo(cf).filename
# print "This is line 5, python says line ", cf.f_lineno 


"""----PATTERNS----"""
#c - code        N - nationality
#C - Country     CN - [country_to_]nationality
#xy (either)
#x_y (separate)
#    {{flagicon|INA}} Indonesian
patflagCc_N = re.compile(r"^\{\{flagicon\|(.+)?\}\}[ ]?([A-Z][A-Za-z/-]+)$")
#        American {{flagicon|USA}}
patN_Cc = re.compile(r"^([A-Z][A-Za-z/-]+)[ ]?\{\{flagicon\|.+?\}\}$")
#    [[Icelanders|Icelandic]]
patC_N = re.compile(r'^\[\[([A-Za-z\- ]*?(\([A-Za-z\- ]+\))?\|([A-Za-z\- ]*?))\]\]$')

#    [[Croatia]]n
patCN = re.compile(r"^((?:\[\[[A-Za-z \-]*?\]\])(?:[a-z]+))$")
#    {{flag|Lithuania}}n
patflagCN = re.compile(r'^((?:\{\{flag\|[[A-Za-z \-]*?\}\})(?:[a-z]+))$')
#    {{flagicon|Jordan}} [[Jordan]]ian
patflagC_CN = re.compile(r'^\{\{flagicon\|([A-Za-z\- ]+?)\}\}[ ]{0,3}\[\[[A-Z][A-Za-z\- ]+\]\][a-z]+$')

#    {{FRA}}-{{BEN}}
patc = re.compile(r'(\{\{[A-Z]{2,3}\}\}){1,3}')
#    [[Taiwan]]
patC = re.compile(r'^\[\[([A-Za-z\- ]*?)\]\]$')
#    {{flag|Canada}}
patflagC = re.compile(r'^\{\{flag\|([A-Za-z -]+?)\}\}$')
#    {{flagicon|Philippines}} [[Philippines]]
patflagC_C = re.compile(r'(\{\{flagicon\|([A-Za-z\- ]+?)\}\})[ ]{0,3}(\[\[[A-Z][A-Za-z\- ]+\]\])$')    
#    {{flagicon|USA}} [[United States|USA]]
patflagc_C_cn = re.compile(r'^\{\{flagicon\|([A-Za-z\- ]+?)\}\}[ ]{0,3}\[\[([A-Za-z\- ]*?(\([A-Za-z\- ]+\))?\|([A-Za-z\- ]*?))\]\]$')

patfinder = re.compile(r"(\[\[[A-Za-z' \-]+\]\](?![a-z]))|(\[\[[A-Za-z' \-]+\]\][a-z]+)|(\[\[[A-Za-z' \-]+\|[A-Za-z' \-]+\]\](?![a-z]))|(\[\[[A-Za-z' \-]+\|[A-Za-z' \-]+\]\][a-z]+)")
#A: [[Czechoslovak]] (patC)
#B: [[Jordan]]ian (patCN)
#C: [[French people|French]] (patC_N)
#D: [[Republic of the Congo|Congo]]lese (patC_CN)
"""----PATTERNS----"""



def decode_nat():    
    final_dict = defaultdict(list)#nationality:[code, country, other]
    file_country = "country_to_nationality.html"
    file_iso = "iso_to_nationality.html"
    #count_dict: country_to_nationality
    count_dict = c_nat.get_country_nation(file_country)
    #code_dict: country_to_code
    code_dict = iso_c.get_code_nation(file_iso) 
    
    nat_to_country = dict()
    code_to_country = dict()
    for k,v in count_dict.items():
        nat_to_country[v] = k
    for k,v in code_dict.items():
        code_to_country[v] = k
    
    for C,N in count_dict.items():
        final_dict[C] = [-1, N]
    for C,c in code_dict.items():
        if C in final_dict.keys():
            final_dict[C][0] = c
        else:
            final_dict[C] = [c, -1]
                
    con = numpy.array(code_dict.keys())
    ctn = numpy.array(count_dict.keys())
    both = numpy.intersect1d(ctn, con) #countries in both (len 38)
    code_to_nat = dict()
    for i in both:
        code_to_nat[code_dict[i]] = count_dict[i]
    return (final_dict, code_to_nat, count_dict, nat_to_country, code_to_country)


def get_infobox_vals(tree):
    #    code_to_nationality, countrySet, codeSet = decode_nat()
    pat = re.compile(r"(<.*>)|({{citation.*)")
    nat = set()
    nat_symbol = set()
    subpat = re.compile(r"\(.*?\)|\-|\|")
    for event, page in tree:
        if page.find('info') != None:
            info = page.find('info')
            for line in info.text.split("\n"):
                if u"nationality" in line.lower():
                    line = re.sub(pat, '', line)
                    l = line.split("=")
                    if l[0].strip() != u"nationality":
                        continue
                    l = filter(None, l)
                    if len(l) > 2:
                        print("len>2:", l)
                    elif len(l) == 2:
                        for n in l:
                            if len(n.strip()) == 0:
                                break
                            nationality = [n.strip() for n in l[1].split("/")]
                            for nation in nationality:
                                if "[[" in nation or "{{" in nation:
                                    nat_symbol.add(nation)
                                elif re.search(subpat,nation):
                                    m = re.sub(subpat, ",", nation)
                                    for n in m.split(","):
                                        if len(n.strip())>0 and n.strip()[0].isupper():
                                            nat.add(n.strip())
                                elif "," in nation:
                                    for n in nation.split(","):
                                        if len(n.strip())>0 and n.strip()[0].isupper():
                                            nat.add(n.strip())
                                else:
                                    nat.add(nation)
                    break
        page.clear()
    return (nat,nat_symbol)
#    natSet, nat_symbol = get_infobox_vals(tree)

def dissect_symbols(word):
    """Get what we want when nationality is something like
    Country Codes: u'{{TUR}}', u'{{PUR}}', u'{{UK}}', u'{{ZAI}}
    Two Names: u'[[United States|American]]', u'[[Turkey|Turkish]]', u'[[Italy|Italian]]'
    Additional: u'[[Iran]]ian', u'[[Argentine]]']
    Miscellaneous (\u20131997) <br> {{COD}} (1997\u2013present)'
    """
    
    return
#        if matched(d, i, (m1,'Cc_N'),(m2,'N_Cc'), (m3,'C_N')):

            
def match(d, line, *args): #m1, m2, m3):
    for m in args:
        if m[0]:
            d[m[1]].append(line)
            return True
    return False

def process_flagCc_N(m, final_dict, code_to_nat, country_to_nat):
    try:
        country = nat_to_country[m.group(2)]
    except:
        print("Error flagCc_N")
    if len(m.group(1)) == 3:
        code_to_nat[m.group(1)] = m.group(2)
        final_dict[country][1] = m.group(2)  #get country for key
        final_dict[country][0] = m.group(1)
    else:
        country_to_nat[m.group(1)] = m.group(2)
        final_dict[country][1] = m.group(2)
    print("---", country, ":", final_dict[country])
        
def extract_symbol_phrase(lines, final_dict, code_to_nat, country_to_nat): #lines is a list

    
    d = defaultdict(list)
    for i in lines:
        m1 = re.match(patflagCc_N,i)
        if m1:
            process_flagCc_N(m1, final_dict, code_to_nat, country_to_nat)
            continue
        m2 = re.match(patN_Cc,i)
        m3 = re.match(patC_N,i)
        if match(d, i,(m2,'N_Cc'), (m3,'C_N')):
            continue  
        m4 = re.match(patCN,i)
        m5 = re.match(patflagCN, i)
        m6 = re.match(patflagC_CN,i)
        if match(d, i, (m4, 'CN'), (m5,'flagCN'),(m6, 'flagC_CN')):
            continue
        m8 = re.match(patC,i)
        m9 = re.match(patflagC, i)
        m10 = re.match(patflagC_C, i)
        m11 = re.match(patflagc_C_cn, i)
        if match(d, i, (m8, 'C'), (m9, 'flagC'), (m10, 'flagC_C'), (m11, 'flagc_C_cn')):
            continue
        m7list = re.findall(patc, i)
        if len(m7list) > 0:
            for i in m7list:
                d['c'].append(i)            
            continue
        m_findlist = re.findall(patfinder, i)
        if len(m_findlist) > 0:
            for find in m_findlist:
                if find[0]:
                    d['C'].append(i)
                elif find[1]:
                    d['CN'].append(i)
                elif find[2]:
                    d['C_N'].append(i)
                elif find[3]:
                    d['C_CN'].append(i)
                else:
                    print("Error: {}".format(i))
            continue
        print("No match error: {}".format(i))
    return d

if __name__ == "__main__":
    
    filename = r"..\bball_finalcombined.xml"
    tree = ET.iterparse(filename,events=('end',), tag='doc')
    
#     countrySet = set()
#     codeSet = set()
#     natSet = set()
#     code_to_nat = dict()
#     country_to_nat = dict()    
#     nat_symbol = set()

    natSet, nat_symbol = get_infobox_vals(tree)
    natSet = set(filter(None, natSet))  ### COULD BE COUNTRY OR NATIONALITY ###
    final_dict, code_to_nat, country_to_nat, nat_to_country, code_to_country = decode_nat()
#    return (final_dict, code_to_nat, count_dict)
    
    print("{:15} {:3}\t".format("code_to_nat", len(code_to_nat)), end="")
    print(" | ".join([str(i) for i in code_to_nat.items()[:3]]))

    print("{:15} {:3}\t".format("country_to_nat", len(country_to_nat)), end="")
    print(" | ".join([str(i) for i in country_to_nat.items()[:3]]))
    
    print("{:15} {:3}: \t".format("final_dict", len(final_dict), end=""))
    print("{}".format(final_dict.items()[::50]))

    print("{:15} {:3}\t".format("natSet", len(natSet)), end="")
    print(" | ".join([str(i) for i in sorted(natSet)[:3]]))
    
    print("{:15} {:3}\t".format("nat_symbol", len(nat_symbol)), end="")
    print(" | ".join([str(i) for i in sorted(nat_symbol)[:3]]))
    
    print("nat_to_country:", sorted(nat_to_country.items()[:3]))
    print("code_to_country:", sorted(code_to_country.items()[:3]))

    symbol_dict = extract_symbol_phrase(list(nat_symbol), final_dict, code_to_nat, country_to_nat)
    """
    flagCc_N
    N_Cc
    C_N
    CN
    flagCN
    flagC_CN
    c 
    C
    C_CN
    flagC
    flagC_C
    flagc_C_cn
    """
    print('----------------------')
    print()
    dict_codes = [v[0] for v in final_dict.values()]
    dict_nats = [v[1] for v in final_dict.values()]
    print(dict_codes[::20])
#     for line in symbol_dict['flagCc_N']:
#         print(line)
#         m = re.match(patflagCc_N, line)
#         try:
#             country = nat_to_country[m.group(2)]
#         except:
#             print("Error flagCc_N")
#             continue
#         if len(m.group(1)) == 3:
#             code_to_nat[m.group(1)] = m.group(2)
#             final_dict[country][1] = m.group(2)  #get country for key
#             final_dict[country][0] = m.group(1)
#         else:
#             country_to_nat[m.group(1)] = m.group(2)
#             final_dict[country][1] = m.group(2)
#         print("---", country, ":", final_dict[country])

            
#         print(m)

        
#     pat = re.compile(r'^((?:\{\{flag\|[[A-Za-z \-]*?\}\})(?:[a-z]+))$')

#         print(m.group(1))
#         print(m.group(2))
#         break

    for a in sorted(symbol_dict, key = lambda a: a.lower()):
        print(a)
        for i in symbol_dict[a][-3:]:
            print("  {}".format(i))
    x = code_to_nat.values()


    #fill_structures(symbol_dict)
    