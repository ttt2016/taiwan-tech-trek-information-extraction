'''
Created on Jul 26, 2016

@author: Janice
'''
from __future__ import print_function
import os
from lxml import etree as ET
import re
from nltk import sent_tokenize
import sys  
reload(sys)  
sys.setdefaultencoding('utf8')
print(sys.getdefaultencoding())

if __name__ == "__main__":
    filename = "../bball_finalcombined.xml"
    tree = ET.iterparse(filename,events=('end',), tag='doc')
    count = 0
    new_root = ET.Element("root")
    #wf = open("bball_nationality_prep.txt",'w')
    for event, page in tree:
        #wf.write("-------\n{}\n".format(page.attrib['title'].encode("utf8")))
        if count==100:
            break
        count += 1
        x = page.text.find("\n")
        x = page.text.find("\n", x+1)
        text = page.text[:2000].encode('utf8')


        
        nationality = ""
        if page.find('info') != None:
            info = page.find('info')
            for line in info.text.split("\n"):
                if "nationality" in line.lower():
                    l = line.split("=")
                    l = filter(None, l)
                    if len(l) == 1:
                        nationality = "N\A"
#                         wf.write("----- N\A\n")
#                         for sent in sent_tokenize(text)[:10]:
#                             wf.write("   {}\n".format(sent.encode('utf8'))) 
                    else:
                        for n in l:
                            nationality = l[1].encode("utf8").strip()
                            if len(nationality) > 1:
                                break
                        if len(nationality.strip()) == 0:
                            nationality = "N\A"
#                         if nationality != "":
#                             wf.write("----- {}\n".format(nationality))
        doc = ET.SubElement(new_root, "doc", title = page.attrib['title'], nationality = nationality.decode('utf8'))
        doc.text = page.text[x:2000]
        page.clear()
    new_tree = ET.ElementTree(new_root)
    new_tree.write("bball_nationality_prep.xml", encoding = 'utf-8', pretty_print = True)
    print("Complete")        
"""
        if nationality != "N\A":
            nat = nationality.split("/")
            for i in range(len(nat)):
                nat[i] = nat[i].strip()
            for sent in sent_tokenize(text.decode('utf8'))[:20]:
                for i in nat:
                    if i in sent.encode('utf8'):
#                         wf.write("\t{}\n".format(sent.encode('unicode')))
                        try:
                            doc.text += "\t{}\n".format(sent.encode('utf8').encode('utf8'))
                        except:
                            print("H")
                            pass
        else:
#             wf.write("----- N\A\n")
            doc.text = page.text[x:2000]
#             for sent in sent_tokenize(text.decode('utf8'))[:10]:
# #                 wf.write("   {}\n".format(sent.encode('utf8')))
#                 try:
#                     doc.text += "\t{}\n".format(sent.encode('utf8')).encode('utf8')
#                 except:
#                     pass
                    
            #wf.write("\n".join(info.text.split()[:10]) )           
        #wf.write("-------------------------------------\n")
        page.clear()
"""
#     wf.close()
