﻿------ Second Sentence
Puerto Rican	He played for the Utah Jazz while in the NBA and the Atléticos de San Germán, Cangrejeros de Santurce, and Capitanes de Arecibo while in the BSN. Ortiz was a member of the Puerto Rican National Team from 1983–2004. 
Puerto Rican	Most notably he was a member of the 2004 Puerto Rican National Basketball Team that defeated the United States. Ortiz was a member of 4 Olympic teams 1988, 1992, 1996, and 2004.
Puerto Rican	Ortiz is also the first Puerto Rican player to be drafted in the NBA. 
Puerto Rican	Ortiz to be the best Puerto Rican basketball player.
Puerto Rican	He also garnered the MVP award in 1979, and, once the three-point shot was established for the first time in the Puerto Rican tournament during the 1980 season, he started making shots from behind the three-point line too.
Puerto Rican	Rodríguez was a member of the Puerto Rican national basketball team, playing in many international tournaments such as the Olympic Games and Pan American Games.
Argentine	Coming from a family of professional basketball players, he is a member of the Argentine men's national basketball team and the San Antonio Spurs in the National Basketball Association (NBA). 
Spanish	Gasol has won two Olympic silver medals, a FIBA World Cup and three EuroBasket titles with the Spanish national basketball team.
Spanish	After moving to the senior team of Barcelona, Gasol played just 25 total minutes in the Spanish ACB League 1998–99 season, and averaged 13.7 minutes per game in the ACB the next year. 
Spanish	Barcelona was victorious in the Spanish National Cup championship game in 2001, and Gasol was named Most Valuable Player. 
Australian	He was the first Australian to play in the NBA, where he played for 10 seasons.
Australian	Luc Longley was born 19 January 1969 in Melbourne, Victoria. At age sixteen Luc was a member of the Australian Under-19 side and the following year, 1986, he joined the Perth Wildcats, with whom he played two games.
Australian	At nineteen he was a member of the national team for the Seoul Olympics, where they came fourth, the best result an Australian senior men's basketball team has achieved in Olympic competition.
Australian	In February 2005, the Phoenix Mercury announced that she had been signed as an assistant coach under fellow Australian and Mercury head coach Carrie Graf.
Australian	Timms played a very influential role in opening the flood gate for many of the future international women's players, especially Australian women basketball stars.
Puerto Rican	Santiago has played for the Puerto Rican national team since 1998 until 2014.