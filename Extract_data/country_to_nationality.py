'''
Created on Aug 1, 2016

@author: Janice

Reads from html source to create a structure
connecting country names with nationality names.
'''

from __future__ import print_function
# from __future__ import division, unicode_literals 
import codecs
from bs4 import BeautifulSoup
import re

def get_country_nation(file_name):
    f=codecs.open(file_name, 'r', 'utf-8')
    document= BeautifulSoup(f.read())
    d = dict()
    country_table = document.findAll('table')[1]
    countries = document.findAll('tr')[4:]
    pat = re.compile(r"(?<!\()((?:[A-Z]{1}[a-z]+)(?:[ -][A-Z]{1}[a-z]+)?(?:[ -][A-Z]{1}[a-z]+)?)")

    try:
        for c in countries: 
            forms = [x.strip() for x in c.get_text().split("\n") if len(x) > 0]
            m = re.search(pat, forms[0])
            m2 = re.search(pat, forms[1])
            d[m.group(0)] = m2.group(0)
    except:
        print('error')
        print(d)
    d["Singapore"] = "Singaporean"
    return d
        
    

if __name__ == "__main__":
    file_name = "country_to_nationality.html"
    d = get_country_nation(file_name)

    for i in sorted(d):
        print("{:20} {}".format(i, d[i]))