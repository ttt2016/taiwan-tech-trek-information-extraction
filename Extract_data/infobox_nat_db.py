'''
Created on Jul 29, 2016

@author: Janice
'''
from __future__ import print_function


###Need to be near these modules to import###
import nat_tree_infobox
######



import os
from lxml import etree as ET
import re
from collections import defaultdict

import country_to_nationality as c_nat
import code_to_country as iso_c
import numpy


from inspect import currentframe, getframeinfo #debug print line number
from scipy.optimize import linesearch
cf = currentframe()
filename = getframeinfo(cf).filename
# print "This is line 5, python says line ", cf.f_lineno 


"""----PATTERNS----"""
#c - code        N - nationality
#C - Country     CN - [country_to_]nationality
#xy (either)
#x_y (separate)
#    {{flagicon|INA}} Indonesian
patflagCc_N = re.compile(r"^\{\{flagicon\|(.+)?\}\}[ ]?([A-Z][A-Za-z/-]+)$")
#        American {{flagicon|USA}}
patN_Cc = re.compile(r"^([A-Z][A-Za-z/-]+)[ ]?\{\{flagicon\|(.+?)\}\}$")
#    [[Icelanders|Icelandic]]
patC_N = re.compile(r'^\[\[([A-Za-z\- ]+?)\|([A-Za-z\- ]+?)\]\]$')

#    [[Croatia]]n
patCN = re.compile(r"^\[\[([A-Za-z \-]*?\]\](?:[a-z]+))$")
#    {{flag|Lithuania}}n
patflagCN = re.compile(r'^(?:\{\{flag\|([[A-Za-z \-]*?\}\}(?:[a-z]+)))$')
#    {{flagicon|Jordan}} [[Jordan]]ian
patflagC_CN = re.compile(r'^\{\{flagicon\|([A-Za-z\- ]+?)\}\}[ ]{0,3}\[\[([A-Z][A-Za-z\- ]+\]\][a-z]+)$')

#    {{FRA}}-{{BEN}}
patc = re.compile(r'(\{\{[A-Z]{2,3}\}\}){1,3}')
#    [[Taiwan]]
patC = re.compile(r'^\[\[([A-Za-z\- ]*?)\]\]$')
#    {{flag|Canada}}
patflagC = re.compile(r'^\{\{flag\|([A-Za-z -]+?)\}\}$')
#    {{flagicon|Philippines}} [[Philippines]]
patflagC_C = re.compile(r'(\{\{flagicon\|([A-Za-z\- ]+?)\}\})[ ]{0,3}(\[\[[A-Z][A-Za-z\- ]+\]\])$')    
#    {{flagicon|USA}} [[United States|USA]]
#patflagc_C_cn = re.compile(r'^\{\{flagicon\|([A-Za-z\- ]+?)\}\}[ ]{0,3}\[\[([A-Za-z\- ]*?(\([A-Za-z\- ]+\))?\|([A-Za-z\- ]*?))\]\]$')
patflagc_C_cn = re.compile(r'^\{\{flagicon\|([A-Za-z\- ]+?)\}\} {0,3}\[\[([A-Za-z\- ]*?)\|([A-Za-z\- ]*?)\]\]$')

patfinder = re.compile(r"(\[\[[A-Za-z' \-]+\]\](?![a-z]))|(\[\[[A-Za-z' \-]+\]\][a-z]+)|(\[\[[A-Za-z' \-\(\)]+\|[A-Za-z' \-]+\]\](?![a-z]))|(\[\[[A-Za-z' \-]+\|[A-Za-z' \-]+\]\][a-z]+)")
#A: [[Czechoslovak]] (patC)
#B: [[Jordan]]ian (patCN)
#C: [[French people|French]] (patC_N)
#D: [[Republic of the Congo|Congo]]lese (patC_CN)
patC_CN = re.compile(r"^\[\[([A-Za-z' \-]+)\|([A-Za-z' \-]+\]\][a-z]+)$")
"""----PATTERNS----"""



def decode_nat():    
    final_dict = defaultdict(list)#nationality:[code, country, other]
    file_country = "country_to_nationality.html"
    file_iso = "iso_to_nationality.html"
    #count_dict: country_to_nationality
    count_dict = c_nat.get_country_nation(file_country)
    #code_dict: country_to_code
    code_dict = iso_c.get_code_nation(file_iso) 
    
    nat_to_country = dict()
    code_to_country = dict()
    for k,v in count_dict.items():
        nat_to_country[v] = k
    for k,v in code_dict.items():
        code_to_country[v] = k
    
    for C,N in count_dict.items():
        final_dict[C] = [-1, N]
    for C,c in code_dict.items():
        if C in final_dict.keys():
            final_dict[C][0] = c
        else:
            final_dict[C] = [c, -1]
                
    con = numpy.array(code_dict.keys())
    ctn = numpy.array(count_dict.keys())
    both = numpy.intersect1d(ctn, con) #countries in both (len 38)
    code_to_nat = dict()
    for i in both:
        code_to_nat[code_dict[i]] = count_dict[i]
    return (final_dict, code_to_nat, count_dict, nat_to_country, code_to_country)

            
def match(d, line, *args): #m1, m2, m3):
    for m in args:
        if m[0]:
            d[m[1]].append(line)
            return True
    return False

def process1(m1, m2, m5, final_dict, code_to_nat, country_to_nat, natSet, nat_to_country):
    if m1: #CountryORCode, Nationality
        natSet.add(m1.group(2))
        if len(m1.group(1)) == 3:
            try:
                country = nat_to_country[m1.group(2)]
            except:
                print("Error flagCc_N")
                return
            code_to_nat[m1.group(1)] = m1.group(2)
            final_dict[country] = [m1.group(1), m1.group(2)]  #get country for key
        else:
            nat_to_country[m1.group(2)] = m1.group(1)
            country_to_nat[m1.group(1)] = m1.group(2)
            final_dict[m1.group(1)][1] = m1.group(2)
    elif m2:
        #Nationality Code
        natSet.add(m2.group(1))
        try:
            country = nat_to_country[m2.group(1)]
        except:
            print("Error N_Cc")
            return
        code_to_country[m2.group(2)] = country
        code_to_nat[m2.group(2)] = m2.group(1)
        final_dict[country] = [m2.group(2), m2.group(1)]
    elif m5:
        #Nation}}ality
        natSet.add(re.sub("\}\}", "", m5.group(1)))

def process2(m6, m11, final_dict, code_to_nat, country_to_nat, natSet, nat_to_country):
    if m6:
        # CountryORCode Nation]]ality
        nat = re.sub("\]\]", "", m6.group(2)).strip()
        natSet.add(nat)
        cc = m6.group(1)
        if len(cc)== 3: #Code Nation]]ality
            code_to_nat[cc] = nat
            try:
                country = nat_to_country[nat]
                final_dict[country][0] = cc
                final_dict[country][1] = nat
            except:
                print("Error C_CN")        
        else: #Country Nation]]ality
            nat_to_country[nat] = m6.group(1)
            country_to_nat[cc] = nat
            if cc in final_dict:
                final_dict[cc][1] = nat
            else:
                final_dict[cc] = [-1, nat]
    elif m11: #3 formats
        country = m11.group(2)        
        if "people" in country: #Code Nat_People Nationality
            natSet.add(m11.group(3))
            code_to_nat[m11.group(1)] = m11.group(3)
            try:
                country = nat_to_country[m11.group(3)]
                if country in final_dict:
                    final_dict[country][1] = m11.group(3)
                else:
                    final_dict[country] = [-1, m11.group(3)]
            except:
                print("ERROR")
                return
        elif len(m11.group(1)) == 3 and len(m11.group(3)) == 3:  #Code Country Code
            if country in final_dict:
                final_dict[country][0] = m11.group(1)
            else:
                final_dict[country] = [m11.group(1),-1]
        #Code Country Nationality
        elif len(m11.group(1)) == 3: 
            nat_to_country[m11.group(3)] = country
            code_to_nat[m11.group(1)] = m11.group(3)
            country_to_nat[country] = m11.group(3)
            final_dict[country] = [m11.group(1), m11.group(3)]
        #Country Country Nationality
        else:
            nat_to_country[m11.group(3)] = country            
            country_to_nat[country] = m11.group(3)
            final_dict[country][1] = m11.group(3)
            natSet.add(m11.group(3))

        
        
def extract_symbol_phrase(lines, final_dict, code_to_nat, country_to_nat, natSet, nat_to_country): #lines is a list    
    d = defaultdict(list)
    for i in lines:
        m1 = re.match(patflagCc_N,i)
        m2 = re.match(patN_Cc,i)
        m5 = re.match(patflagCN, i)    
        if m1 or m2 or m5:
            process1(m1, m2, m5, final_dict, code_to_nat, country_to_nat, natSet, nat_to_country)
            continue
        m6 = re.match(patflagC_CN,i)
        m11 = re.match(patflagc_C_cn, i)
        if m6 or m11:
            process2(m6, m11, final_dict, code_to_nat, country_to_nat, natSet, nat_to_country)
            continue            
        m9 = re.match(patflagC, i)
        m10 = re.match(patflagC_C, i)
        if match(d, i,  (m9, 'flagC'), (m10, 'flagC_C'), (m11, 'flagc_C_cn')):
            continue
        ###MORE LATER ADDED FOR DUAL NATIONALITIES
        m3 = re.match(patC_N,i) 
        m4 = re.match(patCN,i)
        m8 = re.match(patC,i)        
        if match(d, i, (m3,'C_N'), (m4, 'CN'), (m8, 'C')):
            continue  
        m7list = re.findall(patc, i)
        if len(m7list) > 0:
            for i in m7list:
                d['c'].append(i)            
            continue
        m_findlist = re.findall(patfinder, i)
        if len(m_findlist) > 0:
            for find in m_findlist:
                if find[0]:
                    d['C'].append(i)
                elif find[1]:
                    d['CN'].append(i)
                elif find[2]:
                    d['C_N'].append(i)
                elif find[3]:
                    d['C_CN'].append(i)
                else:
                    print("Error: {}".format(i))
            continue
        print("No match error: {}".format(i))
    return d

if __name__ == "__main__":
    
    filename = r"..\bball_finalcombined.xml"
    tree = ET.iterparse(filename,events=('end',), tag='doc')
    
#     countrySet = set()
#     codeSet = set()
#     natSet = set()
#     code_to_nat = dict()
#     country_to_nat = dict()    
#     nat_symbol = set()

    natSet, nat_symbol = nat_tree_infobox.get_infobox_vals(tree)
    natSet = set(filter(None, natSet))  ### COULD BE COUNTRY OR NATIONALITY ###
    final_dict, code_to_nat, country_to_nat, nat_to_country, code_to_country = decode_nat()
#    return (final_dict, code_to_nat, count_dict)
    
    print("{:15} {:3}\t".format("code_to_nat", len(code_to_nat)), end="")
    print(" | ".join([str(i) for i in code_to_nat.items()[:3]]))

    print("{:15} {:3}\t".format("country_to_nat", len(country_to_nat)), end="")
    print(" | ".join([str(i) for i in country_to_nat.items()[:3]]))
    
    print("{:15} {:3}: \t".format("final_dict", len(final_dict), end=""))
    print("{}".format(final_dict.items()[::50]))

    print("{:15} {:3}\t".format("natSet", len(natSet)), end="")
    print(" | ".join([str(i) for i in sorted(natSet)[:3]]))
    
    print("{:15} {:3}\t".format("nat_symbol", len(nat_symbol)), end="")
    print(" | ".join([str(i) for i in sorted(nat_symbol)[:3]]))
    
    print("nat_to_country:", sorted(nat_to_country.items()[:3]))
    print("code_to_country:", sorted(code_to_country.items()[:3]))

    symbol_dict = extract_symbol_phrase(list(nat_symbol), final_dict, code_to_nat, country_to_nat, natSet, nat_to_country)

    for line in symbol_dict['C_CN']: #Country Nation]]ality
        m = re.search(patC_CN, line)
        t = m.group(2).split("]]")
        nat = "".join(t)
        country = t[0]
        natSet.add(nat)
        nat_to_country[nat] = country
        country_to_nat[country] = nat
        if country in final_dict:
            final_dict[country][1] = nat
        else:
            final_dict[country] = [-1, nat]
    del symbol_dict['C_CN']
    
    pat = re.compile(r"\[\[([A-Za-z \-\(\)\']+)\|([A-Za-z \-\(\)\']+)\]\]")
    rm_pat = re.compile(r"\(.+?\)|people")
    for line in symbol_dict['C_N']:
        l = re.sub(rm_pat, "", line)
        m = re.findall(pat,l)
        for i in m: #NatPluralORCountry Nationality
            natSet.add(i[1])
            temp = i[1] + 's'
            if i[0].endswith(temp):
                continue
            if i[0] not in final_dict:
                final_dict[i[0]] = [-1, i[1]]
            else:
                final_dict[i[0]][1] = i[1]
    del symbol_dict['C_N']
    
    pat = re.compile(r"\[\[([A-Za-z ]+\]\])[a-z]+")
    for line in symbol_dict['CN']: #nation]]ality
        m = pat.search(line)
        t = m.group(1).split("]]")
        nat = "".join(t)
        natSet.add(nat)
        country = t[0]
        nat_to_country[nat] = country
        country_to_nat[country] = nat 
        if country in final_dict:
            final_dict[country][1] = nat 
        else:
            final_dict[country] = [-1, nat]
    del symbol_dict['CN']
    
    for i in symbol_dict.items():
        print(i)
    #remaining ('c', 'flagC', 'C', 'flagC_C')
    #                --> only have country info

    
#     WRITTEN    ##############################

    code_to_nat2 = dict()
    for i in sorted(final_dict):
        if final_dict[i][0] != -1 and final_dict[i][1] != -1:
            code_to_nat2[final_dict[i][0]] = final_dict[i][1]
    for i in code_to_nat.keys():
        if i not in code_to_nat2.keys():
            code_to_nat2[i] = code_to_nat[i]

    country_to_nat2 = dict()
    for i in sorted(final_dict):
        if final_dict[i][1] != -1:
            country_to_nat2[i] = final_dict[i][1]
    for i in country_to_nat.keys():
        if i not in country_to_nat2.keys():
            country_to_nat2[i] = country_to_nat[i]


    fullfile = open("rel_full.txt","w")
    fullfilen = open("rel_full_neat.txt","w")
    for i in sorted(final_dict):
        fullfile.write(u"{}\t{}\t{}\n".format(i, final_dict[i][0], final_dict[i][1]).encode('utf8'))
        fullfilen.write(u"{:20} {:3}  {:<20}\n".format(i, final_dict[i][0], final_dict[i][1]).encode('utf8'))
    fullfile.close()
    fullfilen.close()
      
#     codefile = open("rel_code_nat.txt","w")
#     for i in code_to_nat2:
#         codefile.write(u"{:3}\t{}\n".format(i, code_to_nat2[i]))
#     codefile.close()
#      
#     countryfile = open("rel_country_nat.txt", "w")
#     countryfilen = open("rel_country_nat_neat.txt", "w")
# 
#     for i in country_to_nat2:
#         countryfile.write(u"{}\t{}\n".format(i, country_to_nat2[i]))
#         countryfilen.write(u"{:25} {}\n".format(i, country_to_nat2[i]))