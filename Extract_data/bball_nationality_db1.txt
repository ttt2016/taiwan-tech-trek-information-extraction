American	Kareem Abdul-Jabbar (born Ferdinand Lewis Alcindor, Jr.; April 16, 1947) is an American retired professional basketball player who played 20 seasons in the National Basketball Association (NBA) for the Milwaukee Bucks and Los Angeles Lakers. 
American	Michael Jeffrey Jordan (born February 17, 1963), also known by his initials, MJ, is an American retired professional basketball player. He is also a businessman, and principal owner and chairman of the Charlotte Hornets. 
American	Earvin "Magic" Johnson Jr. (born August 14, 1959) is a retired American professional basketball player who played point guard for the Los Angeles Lakers of the National Basketball Association (NBA) for 13 seasons. 
American	Robert Sylvester Kelly (born January 8, 1967), known professionally as R. Kelly, is an American recording artist, songwriter, record producer, and former professional basketball player. 
American	Marion Lois Jones (born October 12, 1975), also known as Marion Jones-Thompson, is an American former world champion track and field athlete and a former professional basketball player for Tulsa Shock in the WNBA
Puerto Rican	Mario Morales Micheo (born November 13, 1957), a.k.a. "Quijote" Morales for his ability to conquer both scoring and team championships in Puerto Rico's BSN league, is a Puerto Rican basketball player.
American	Denzel Hayes Washington, Jr. (born December 28, 1954) is an American actor and filmmaker. 
Puerto Rican	Jos� Rafael Ortiz (born October 25, 1963), better known as Picul�n or Picu, is a retired Puerto Rican professional basketball player. He has played in the NCAA, NBA, various European teams and the National Superior Basketball League of Puerto Rico.
Puerto Rican	Rub�n Rodr�guez (born August 5, 1953 in New York, New York) is a Puerto Rican former basketball player. 
Argentine	Emanuel David "Manu" Gin�bili Maccari (, ; born 28 July 1977) is an Argentine professional basketball player for the San Antonio Spurs of the National Basketball Association (NBA).
American	Shaquille Rashaun O'Neal ( ; born March 6, 1972), nicknamed Shaq ( ), is a retired American professional basketball player who is currently an analyst on the television program "Inside the NBA". 
American	Marcus D. Camby (born March 22, 1974) is an American former professional basketball player. He was named Defensive Player of the Year during the 2006�07 NBA season, leading the league in blocked shots per game. 
American	Peter Press "Pistol Pete" Maravich (June 22, 1947 � January 5, 1988) was an American professional basketball player. 
Spanish	Pau Gasol S�ez (, ; born July 6, 1980) is a Spanish professional basketball player for the Chicago Bulls of the National Basketball Association (NBA). 
Ameircan	John MacBeth Paxson (born September 29, 1960) is an American basketball administrator and former player who has been Vice President of Basketball Operations for the Chicago Bulls of the National Basketball Association since 2009, after serving as team general manager from 2003 to 2009.
Australian	Lucien James "Luc" Longley (born 19 January 1969) is an Australian retired professional basketball player. 
Australian	Michele Margaret Timms (born 28 June 1965 in Melbourne, Victoria) is an Australian female basketball coach and retired professional basketball player who played for the Phoenix Mercury in the Women's National Basketball Association. 
American	Mark Ellsworth Madsen (born January 28, 1976) is an American former professional basketball player and current assistant coach of the Los Angeles Lakers of the National Basketball Association (NBA). 
American	Charles Wade Barkley (born February 20, 1963) is an American retired professional basketball player and current analyst on the television program "Inside the NBA". 
American	David Maurice Robinson (born August 6, 1965) is an American former professional basketball player, who played center for the San Antonio Spurs in the National Basketball Association (NBA) for his entire career.



------ Dual
Puerto Rican,American	Jerome Alfred Mincy Clark (born October 10, 1964) is a Puerto Rican professional basketball player of American parents. Mincy has played in the NCAA with the UAB Blazers and the National Superior Basketball League of Puerto Rico (BSN) with Bayam�n Cowboys. 
American,Puerto Rican	Daniel Gregg Santiago (born June 24, 1976) is an American-born Puerto Rican professional basketball player for the Cangrejeros de Santurce. 
American,Jamaican	Patrick Aloysius Ewing, Sr. (born August 5, 1962) is a Jamaican-American retired Hall of Fame basketball player. 
Congolese,American	Dikembe Mutombo Mpolondo Mukamba Jean-Jacques Wamutombo (born June 25, 1966), commonly referred to as Dikembe Mutombo, is a Congolese American retired professional basketball player who played 18 seasons in the National Basketball Association (NBA).
Nigerian,American	Hakeem Abdul Olajuwon (; ; born January 21, 1963), formerly known as Akeem Olajuwon, is a Nigerian-American retired professional basketball player
American,German	Shawn Paul Bradley (born March 22, 1972) is an American and German (dual citizen) retired basketball player who played center for the Philadelphia 76ers, the New Jersey Nets and the Dallas Mavericks in the National Basketball Association (NBA). 
Sudanese,Amrican	Manute Bol (; October 16, 1962 � June 19, 2010) was a Sudanese-born American basketball player and political activist.
------ Second Sentence
Puerto Rican	He played for the Utah Jazz while in the NBA and the Atl�ticos de San Germ�n, Cangrejeros de Santurce, and Capitanes de Arecibo while in the BSN. Ortiz was a member of the Puerto Rican National Team from 1983�2004. 
Puerto Rican	Most notably he was a member of the 2004 Puerto Rican National Basketball Team that defeated the United States. Ortiz was a member of 4 Olympic teams 1988, 1992, 1996, and 2004.
Puerto Rican	Ortiz is also the first Puerto Rican player to be drafted in the NBA. 
Puerto Rican	Ortiz to be the best Puerto Rican basketball player.
Puerto Rican	He also garnered the MVP award in 1979, and, once the three-point shot was established for the first time in the Puerto Rican tournament during the 1980 season, he started making shots from behind the three-point line too.
Puerto Rican	Rodr�guez was a member of the Puerto Rican national basketball team, playing in many international tournaments such as the Olympic Games and Pan American Games.
Argentine	Coming from a family of professional basketball players, he is a member of the Argentine men's national basketball team and the San Antonio Spurs in the National Basketball Association (NBA). 
Spanish	Gasol has won two Olympic silver medals, a FIBA World Cup and three EuroBasket titles with the Spanish national basketball team.
Spanish	After moving to the senior team of Barcelona, Gasol played just 25 total minutes in the Spanish ACB League 1998�99 season, and averaged 13.7 minutes per game in the ACB the next year. 
Spanish	Barcelona was victorious in the Spanish National Cup championship game in 2001, and Gasol was named Most Valuable Player. 
Australian	He was the first Australian to play in the NBA, where he played for 10 seasons.
Australian	Luc Longley was born 19 January 1969 in Melbourne, Victoria. At age sixteen Luc was a member of the Australian Under-19 side and the following year, 1986, he joined the Perth Wildcats, with whom he played two games.
Australian	At nineteen he was a member of the national team for the Seoul Olympics, where they came fourth, the best result an Australian senior men's basketball team has achieved in Olympic competition.
Australian	In February 2005, the Phoenix Mercury announced that she had been signed as an assistant coach under fellow Australian and Mercury head coach Carrie Graf.
Australian	Timms played a very influential role in opening the flood gate for many of the future international women's players, especially Australian women basketball stars.
Puerto Rican	Santiago has played for the Puerto Rican national team since 1998 until 2014.
Puerto Rican




------ Not what we want - Chink?
Serbian	Maravich's father, Petar "Press" Maravich, the son of Serbian immigrants and a former professional player-turned-coach, showed him the fundamentals starting when he was seven years old. 
Spanish	In 2005, Santiago signed for a season with Spanish ACB League's Unicaja M�laga. 