'''
Created on Aug 1, 2016

@author: Janice
'''
from __future__ import print_function
import country_to_nationality as c_nat
import code_to_country as iso_c
import numpy

if __name__ == "__main__":
    file_country = "country_to_nationality.html"
    file_iso = "iso_to_nationality.html"
    count_dict = c_nat.get_country_nation(file_country)
    code_dict = iso_c.get_code_nation(file_iso)
    
    count = count_dict.keys()
    code = code_dict.keys()
    ctn = numpy.array(count)
    con = numpy.array(code)
    
    both = numpy.intersect1d(ctn, con) #len 38
    #in country list only
    count = numpy.setdiff1d(count, con) #len 36
    #in code list only
    code = numpy.setdiff1d(con, ctn) #len 108
    print(len(both))
    print(len(count))
    print(len(code))
    
    print(code_dict.items()[0])
    print(count_dict.items()[0])
    result = dict()
    for i in both:
        result[code_dict[i]] = count_dict[i]
    print(result)
    
#     while len(count) > 0 and len(code) > 0:
#         if len(count) == 0:
#             print(u"\t\t{}".format(code[0]))
#         elif len(code) == 0:
#             print(u"\t{}".format(count[0]))
#         elif count[0] < code[0]:
#             print(u"\t{}".format(count[0]))
#             count = numpy.delete(count,0)
#         else:
#             print(u"\t\t{}".format(code[0]))
#             code = numpy.delete(code,0)

#     full = []
#     for i in count:
#         full.append(('country', i))
#     for i in code:
#         full.append(('code', i))
#     for i in both:
#         full.append(('---', i))
#     for a,b in sorted(full, key = lambda x: x[1]):
#         print(u"{:10}{}".format(a,b))
#     
    

    
    
    

"""
1
re.compile(r"^\{\{flagicon\|.+?\}\}[ ]?([A-Z][A-Za-z/-]+)$")
Group 1
    {{flagicon|[COUNTRY or (code)]}}[NATIONALITY]
    {{flagicon|INA}} Indonesian
    {{flagicon|Philippines}} Filipino'
    {{flagicon|Philippines}} Filipino
    {{flagicon|PHI}}Filipino
    
2  
re.compile(r"^([A-Z][A-Za-z/-]+)[ ]?\{\{flagicon|.+?\}\}$")
Group 1
    [NATIONALITY]{{flagicon|[COUNTRY]
    American {{flagicon|USA}}
3 
re.compile(r'(\{\{[A-Z]{2,3}\}\}){1,3}')
*Use findall (If len(list) > 0)
    [nation_CODE(s)]
    {{ZAI}} (-1997)  {{COD}} (1997-present)
    {{FRA}}-{{BEN}}
    {{KSA}}
4
re.compile(r"^((?:\[\[[A-Za-z \-]*?\]\])(?:[a-z]+))$")
*Use sub to get:
Group 1
    re.sub(r"\[|\]","",m.group(1))
    [country_to_]nationality
    [[Croatia]]n
    [[Israel]]i
5
Group 2  (the nationality)
re.compile(r'^\[\[([A-Za-z\- ]*?(\([A-Za-z\- ]+\))?\|([A-Za-z\- ]*?))\]\]$')
    [[[COUNTRY]|[NATIONALITY]]
    [[Icelanders|Icelandic]]
    [[Morocco|Moroccan]]
    [[United Kingdom|British]]
    [[Spain|Spanish]]
    [[France|French]]
6
Group 1 the country
re.compile(r'^\[\[([A-Za-z\- ]*?)\]\]$')
    [[[COUNTRY]]]
    [[Taiwan]]
    [[Belgian]]
    [[Mali]]
    [[France]]
7
re.compile(r'^((?:\{\{[a-z]*?\|[[A-Za-z \-]*?\}\})(?:[a-z]+))$')
re.sub("\{\{[a-zA-Z\-]*?\||\}\}","",m.group(1))
    {{flag|[COUNTRY_to_]}}[Nationality]
    {{flag|Lithuania}}n
8
re.compile(r'^\{\{flag\|([A-Za-z -]+?)\}\}$')
    {{flag|[COUNTRY]}}
    {{flag|Canada}}
    {{flag|Philippines}}
    {{flag|Germany}}
    
9
re.compile(r'(\{\{flagicon\|([A-Za-z\- ]+?)\}\})[ ]{0,3}(\[\[[A-Z][A-Za-z\- ]+\]\])$')    {{flagicon|[COUNTRY]}} [[COUNTRY]]
    {{flagicon|Philippines}} [[Philippines]]
10
re.compile(r'^\{\{flagicon\|([A-Za-z\- ]+?)\}\}[ ]{0,3}\[\[[A-Z][A-Za-z\- ]+\]\][a-z]+$')
    {{flagicon|[COUNTRY]}} [COUNTRY_TO_]NATIONALITY
    {{flagicon|Syria}} [[Syria]]n
    {{flagicon|Jordan}} [[Jordan]]ian
    {{flagicon|Estonia}} [[Estonia]]n
    {{flagicon|Israel}} [[Israel]]i
11
re.compile(r'^\{\{flagicon\|([A-Za-z\- ]+?)\}\}[ ]{0,3}\[\[([A-Za-z\- ]*?(\([A-Za-z\- ]+\))?\|([A-Za-z\- ]*?))\]\]$')
    {{flagicon|[CODE]}} [[COUNTRY|code_or_nationality]}
    {{flagicon|USA}} [[United States|USA]]
    {{flagicon|TUR}} [[Turkey|Turkish]]
    {{flagicon|GBR}} [[Great Britain|GBR]]

12
re.compile(r"(\[\[[A-Za-z' \-]+\]\](?![a-z]))|(\[\[[A-Za-z' \-]+\]\][a-z]+)|(\[\[[A-Za-z' \-]+\|[A-Za-z' \-]+\]\](?![a-z]))|(\[\[[A-Za-z' \-]+\|[A-Za-z' \-]+\]\][a-z]+)")
a = [] 
[[Czechoslovak]]
[[Jordan]]
[[Kuwait]]
b = []
[[Jordan]]ian
[[Serbia]]n
[[Senegal]]esec = []
[[Czech Republic|Czech]]
[[Montenegro|Montenegrin]]
[[Serbia|Serbian]]
[[United States|American]]
[[Dominican Republic|Dominican]]
[[Germany|German]]
c = []
[[French people|French]]
[[Montenegro|Montenegrin]]
[[People's Republic of China|Chinese]]
[[France|French]]
d = []
[[Republic of the Congo|Congo]]lese
[[FYR Macedonia|Macedonia]]n
for i in text:
    matches = re.findall(pat12,i)
    for m in matches:
        if m[0]:
            a.append(m[0])
        elif m[1]:
            b.append(m[1])
        elif m[2]:
            c.append(m[2])
        elif m[3]:
            d.append(m[3])
"""
################ DELETE BELOW ##############
    
#c - code        N - nationality
#C - Country     CN - [country_to_]nationality
#xy (either)
#x_y (separate)
#    {{flagicon|INA}} Indonesian
patflagCc_N = re.compile(r"^\{\{flagicon\|.+?\}\}[ ]?([A-Z][A-Za-z/-]+)$")
#        American {{flagicon|USA}}
patN_Cc = re.compile(r"^([A-Z][A-Za-z/-]+)[ ]?\{\{flagicon\|.+?\}\}$")
#    [[Icelanders|Icelandic]]
patC_N = re.compile(r'^\[\[([A-Za-z\- ]*?(\([A-Za-z\- ]+\))?\|([A-Za-z\- ]*?))\]\]$')

#    [[Croatia]]n
patCN = re.compile(r"^((?:\[\[[A-Za-z \-]*?\]\])(?:[a-z]+))$")
#    {{flag|Lithuania}}n
patflagCN = re.compile(r'^((?:\{\{flag\|[[A-Za-z \-]*?\}\})(?:[a-z]+))$')
#    {{flagicon|Jordan}} [[Jordan]]ian
patflagC_CN = re.compile(r'^\{\{flagicon\|([A-Za-z\- ]+?)\}\}[ ]{0,3}\[\[[A-Z][A-Za-z\- ]+\]\][a-z]+$')

#    {{FRA}}-{{BEN}} ### MATCHLIST 7 ###
patc = re.compile(r'(\{\{[A-Z]{2,3}\}\}){1,3}')
#    [[Taiwan]]
patC = re.compile(r'^\[\[([A-Za-z\- ]*?)\]\]$')
#    {{flag|Canada}}
patflagC = re.compile(r'^\{\{flag\|([A-Za-z -]+?)\}\}$')
#    {{flagicon|Philippines}} [[Philippines]]
patflagC_C = re.compile(r'(\{\{flagicon\|([A-Za-z\- ]+?)\}\})[ ]{0,3}(\[\[[A-Z][A-Za-z\- ]+\]\])$')    
#    {{flagicon|USA}} [[United States|USA]]
patflagc_C_cn = re.compile(r'^\{\{flagicon\|([A-Za-z\- ]+?)\}\}[ ]{0,3}\[\[([A-Za-z\- ]*?(\([A-Za-z\- ]+\))?\|([A-Za-z\- ]*?))\]\]$')

patfinder = re.compile(r"(\[\[[A-Za-z' \-]+\]\](?![a-z]))|(\[\[[A-Za-z' \-]+\]\][a-z]+)|(\[\[[A-Za-z' \-]+\|[A-Za-z' \-]+\]\](?![a-z]))|(\[\[[A-Za-z' \-]+\|[A-Za-z' \-]+\]\][a-z]+)")
#A: [[Czechoslovak]] (patC)
#B: [[Jordan]]ian (patCN)
#C: [[French people|French]] (patC_N)
#D: [[Republic of the Congo|Congo]]lese (patC_CN)


'''
code_to_nat 72
  EGY,Egyptian
  HND,Honduran
  BEL,Belgian
country_to_nat 75
  Brazil,Brazilian
  Afghanistan,Afghan
  Italy,Italian
countrySet 75
  Afghanistan
  Argentina
  Australia
codeSet 246
  Afghanistan
  Aland Islands
  Albania
c
  {{USA}}
  {{TUR}}
  {{CAN}}
flagC_CN
  {{flagicon|Estonia}} [[Estonia]]n
  {{flagicon|Israel}} [[Israel]]i
  {{flagicon|EST}} [[Estonia]]n
flagC
  {{flag|Canada}}
  {{flag|Philippines}}
  {{flag|Germany}}
CN
  [[Iceland]]ic
  [[Estonia]]n
  [[Uruguay]]an
N_Cc
  American {{flagicon|USA}}
C_CN
  [[Republic of the Congo|Congo]]lese
  [[FYR Macedonia|Macedonia]]n
flagCN
  {{flag|Lithuania}}n
C
  [[Tunisia]]
  [[United Arab Emirates]]
  [[Qatar]]
C_N
  [[Bulgaria|Bulgarian]]
  [[France|French]]
  [[Azerbaijanis|Azerbaijani]]
flagC_C
  {{flagicon|Philippines}} [[Philippines]]
flagc_C_cn
  {{flagicon|CAN}} [[Canada|Canadian]]
  {{flagicon|Greece}} [[Greece|Greek]]
  {{flagicon|United States}} [[United States|American]]
flagCc_N
  {{flagicon|KOR}}Korean
  {{flagicon|USA}} American
  {{flagicon|PHI}} Filipino
'''