'''
Created on Jul 26, 2016

@author: Janice
'''

from __future__ import print_function

from lxml import etree as ET
import re


if __name__ == "__main__":
    filename = "../bball_finalcombined.xml"
    tree = ET.iterparse(filename,events = ('end',),tag='doc')
    idict = dict()
    count = 0
    position_count = 0
    pdict = dict()
    for i in ["point guard", "shooting guard", "small forward", "power forward", "center", "forward", "guard"]:
        pdict[i] = 0
    pat = re.compile(r'(point guard|shooting guard|small forward|power forward|center|forward|guard)', re.I)
    for event, page in tree:
        count += 1
        info = page.find('info')
        m = re.search(pat, page.text)
        if m != None:
            pdict[m.group(0).lower()]+=1
            position_count += 1
        if info != None:
            for line in info.text.split("\n"):
                if len(line) == 0 or "=" not in line:
                    continue
                l = filter(None, line.strip().split("="))
                l[0] = l[0].strip().lower()
                if len(l) >1:
                    l[1] = l[1].strip("[] ")
#                 if l[0] == "birth_date" and not len(l) > 1:
#                     print(line)
                if len(l) > 1:
                    if l[0] in idict:
                        idict[l[0]][0] += 1
                    else:
                        idict[l[0]] = [1,0]
                    if l[1] in page.text:
                        idict[l[0]][1] += 1


    #sorted(data, key=data.get)
    attribs = sorted(idict, key = idict.get, reverse =True)
    newfile = open("top_bball_attrib.txt", "w")
    newfile.write("{:5} articles total\n".format(count))
    newfile.write("{:5} articles total w/ positions inside\n".format(position_count))
    for i in sorted(pdict, key = pdict.get, reverse=True):
        newfile.write("{:5} {}\n".format(pdict[i], i))
    newfile.write("------------------{:5}{:5}\n".format("info", "text"))
    for i in attribs[:30]:
        newfile.write("{:5}{:5} {}\n".format(idict[i][0], idict[i][1], i))
    newfile.close()
    print("Complete")
            
    