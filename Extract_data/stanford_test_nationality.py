'''
Created on Jul 26, 2016

@author: Janice
'''
from __future__ import print_function
from nltk.tag.stanford import StanfordNERTagger
from nltk.tag.stanford import StanfordPOSTagger
from nltk.tokenize import word_tokenize
from nltk.tokenize import StanfordTokenizer
from collections import defaultdict
from nltk import  ne_chunk
# import os
import nltk



def print_(strlist):
    for i in strlist:
        print(i)
        
        
def examine(file, tokenizer, st, sp):
    x = file.readlines()

    db = [y.strip().split("\t") for y in x]

#     text = db[0][1]
    pd = defaultdict(dict)
    nd = defaultdict(dict)
#             #Morales
#             #Jos
#             #Rub
    fix = ["Mario M", "Jos", "Rub"]
    for country, text in db:
#         if not (text.startswith(fix[0]) or text.startswith(fix[1]) or text.startswith(fix[2])):
#             continue  
#         print(country)
#         print(text)
        
#         x = -1
        if country not in text:
            continue
        tok = tokenizer.tokenize(text)
        postag = sp.tag(tok)
        nertag = st.tag(tok)
        country_name = country.split()
        start = 0
        try:
            if len(country_name) == 1:
                x = tok.index(country_name[0])
            else:
                start = 0
                while start < len(tok):
                    x = tok.index(country_name[0], start)
                    if " ".join(country_name) == " ".join(tok[x:x+len(country_name)]):
                        break
                    else:
                        start = x+1                
            pos = postag[max(0,x-2):x+2+len(country_name)]
            ner = nertag[max(0,x-1):x+1+len(country_name)]
            print(pos)
            print(ner)
            pt = tuple(y[1] for y in pos)
            nt = tuple(y[1] for y in ner)
            phrase = " ".join([y[0] for y in pos])
#             pd[pt].add(" ".join([y[0] for y in pos]))
#             nd[nt].add(" ".join([y[0] for y in pos]))
            if phrase not in pd[pt]:
                pd[pt][phrase] = 1
            else:
                pd[pt][phrase] +=1
            if phrase not in nd[nt]:
                nd[nt][phrase] = 1
            else:
                nd[nt][phrase] += 1
        except ValueError:
            print("ValueError")
        except:
            print("No match")
            print(postag)
            print(nertag)

    wf = open("nat_dict_patterns.txt", "w")
    wf.write("POS\n")
    print(dict(pd))
#     for i in sorted(dict(pd).items(), key = lambda x: sum(x[1].values()), reverse = True):
#         wf.write("{:3} {}\n{}\n".format(sum(i[1].values()), i[0], i[1]))
    #print(dict(pd))
    #print(dict(nd))
    for i in sorted(pd.items(), key = lambda x: sum(x[1].values()), reverse = True):
        wf.write("{:3} {}\n{}\n".format(sum(i[1].values()), i[0], i[1]))
    wf.write("NER\n")
    for i in sorted(nd.items(), key = lambda x: sum(x[1].values()), reverse = True):
        wf.write("{:3} {}\n{}\n".format(sum(i[1].values()), i[0], i[1]))        
#     for i in dict(nd):
#         wf.write("{:3} {}\n{}\n".format(sum(nd[i].values()), i, nd[i]))
    wf.close()
            
"""
SEGMENT OF OUTPUT:
[(u'John', u'NNP'), (u'MacBeth', u'NNP'), (u'Paxson', u'NNP'), (u'-LRB-', u'-LRB-'), (u'born', u'VBN'), (u'September', u'NNP'), (u'29', u'CD'), (u',', u','), (u'1960', u'CD'), (u'-RRB-', u'-RRB-'), (u'is', u'VBZ'), (u'an', u'DT'), (u'American', u'JJ'), (u'basketball', u'NN'), (u'administrator', u'NN'), (u'and', u'CC'), (u'former', u'JJ'), (u'player', u'NN'), (u'who', u'WP'), (u'has', u'VBZ'), (u'been', u'VBN'), (u'Vice', u'NNP'), (u'President', u'NNP'), (u'of', u'IN'), (u'Basketball', u'NNP'), (u'Operations', u'NNP'), (u'for', u'IN'), (u'the', u'DT'), (u'Chicago', u'NNP'), (u'Bulls', u'NNPS'), (u'of', u'IN'), (u'the', u'DT'), (u'National', u'NNP'), (u'Basketball', u'NNP'), (u'Association', u'NNP'), (u'since', u'IN'), (u'2009', u'CD'), (u',', u','), (u'after', u'IN'), (u'serving', u'VBG'), (u'as', u'IN'), (u'team', u'NN'), (u'general', u'JJ'), (u'manager', u'NN'), (u'from', u'IN'), (u'2003', u'CD'), (u'to', u'TO'), (u'2009', u'CD'), (u'.', u'.')]
[(u'John', u'PERSON'), (u'MacBeth', u'PERSON'), (u'Paxson', u'PERSON'), (u'-LRB-', u'O'), (u'born', u'O'), (u'September', u'O'), (u'29', u'O'), (u',', u'O'), (u'1960', u'O'), (u'-RRB-', u'O'), (u'is', u'O'), (u'an', u'O'), (u'American', u'O'), (u'basketball', u'O'), (u'administrator', u'O'), (u'and', u'O'), (u'former', u'O'), (u'player', u'O'), (u'who', u'O'), (u'has', u'O'), (u'been', u'O'), (u'Vice', u'O'), (u'President', u'O'), (u'of', u'O'), (u'Basketball', u'O'), (u'Operations', u'O'), (u'for', u'O'), (u'the', u'O'), (u'Chicago', u'ORGANIZATION'), (u'Bulls', u'ORGANIZATION'), (u'of', u'ORGANIZATION'), (u'the', u'ORGANIZATION'), (u'National', u'ORGANIZATION'), (u'Basketball', u'ORGANIZATION'), (u'Association', u'ORGANIZATION'), (u'since', u'O'), (u'2009', u'O'), (u',', u'O'), (u'after', u'O'), (u'serving', u'O'), (u'as', u'O'), (u'team', u'O'), (u'general', u'O'), (u'manager', u'O'), (u'from', u'O'), (u'2003', u'O'), (u'to', u'O'), (u'2009', u'O'), (u'.', u'O')]
[(u'is', u'VBZ'), (u'an', u'DT'), (u'Australian', u'JJ'), (u'retired', u'JJ'), (u'professional', u'JJ')]
[(u'an', u'O'), (u'Australian', u'O'), (u'retired', u'O')]
[(u'is', u'VBZ'), (u'an', u'DT'), (u'Australian', u'JJ'), (u'female', u'JJ'), (u'basketball', u'NN')]
[(u'an', u'O'), (u'Australian', u'O'), (u'female', u'O')]
[(u'is', u'VBZ'), (u'an', u'DT'), (u'American', u'JJ'), (u'former', u'JJ'), (u'professional', u'JJ')]
[(u'an', u'O'), (u'American', u'O'), (u'former', u'O')]
[(u'is', u'VBZ'), (u'an', u'DT'), (u'American', u'JJ'), (u'retired', u'JJ'), (u'professional', u'JJ')]
[(u'an', u'O'), (u'American', u'O'), (u'retired', u'O')]
[(u'is', u'VBZ'), (u'an', u'DT'), (u'American', u'JJ'), (u'former', u'JJ'), (u'professional', u'JJ')]
[(u'an', u'O'), (u'American', u'O'), (u'former', u'O')]
"""


        

#     tok = tokenizer.tokenize(text)
#     postag = sp.tag(tok)
#     nertag = st.tag(tok)
#     print("TOK")
#     print(tok)
#     print("POSTAG")
#     print(postag)
#     print("NERTAG")
#     print(nertag)


    #print(st.tag(tok)) 
# NER
# 3 class:    Location, Person, Organization
# 4 class:    Location, Person, Organization, Misc
# 7 class:    Location, Person, Organization, Money, Percent, Date, Time    for i in st.tag(tok):

#     st = nltk.StanfordPOSTagger(#"../stanford-ner/classifiers/english.all.3class.distsim.crf.ser.gz",
#            "../stanford-pos/stanford-postagger-3.6.0.jar")

    
#     sentToken = st.tag(text)
#     print(sentToken)
#     #S = [x**2 for x in range(10)]
#     #s = [(a,b) for x.split("\t") in file.readlines()]
#     #print(s)


if __name__ == "__main__":
    base = "bball_nat_db_"
    dualf = base + "dual1.txt"
    nof = base + "no1.txt"
    normf = base + "norm1.txt"
    secondf = base + "second1.txt"


    st = StanfordNERTagger('../stanford-ner/classifiers/english.all.3class.distsim.crf.ser.gz',
                       '../stanford-ner/stanford-ner.jar')#, encoding = 'utf-8')
    sp = StanfordPOSTagger('../stanford-postagger/models/english-bidirectional-distsim.tagger',
                           '../stanford-postagger/stanford-postagger.jar')#, encoding = 'utf-8')
    word_tokenize = StanfordTokenizer('../stanford-postagger/stanford-postagger.jar')#, encoding='utf-8')

    
    with open(normf, "r") as nf:
        nf.readline()
        examine(nf, word_tokenize, st, sp)
    print("complete")
    
    
'''
    sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
    sent_tokenize = sent_detector.tokenize("""Brunson has attributed his cure to the prayers of\
     friends of his wife and their correspondence with Kathryn Kuhlman, a self-proclaimed \
     Christian faith healer. Louise developed a tumor shortly afterwards and, when she went \
     for surgery, her tumor was also found to have disappeared. In 1975, their daughter Doyla \
     was diagnosed with scoliosis, yet her spine straightened completely within three months. \
     Doyla died at 18 of a heart-valve condition. His son, Todd, also plays poker professionally.""")
    print(sent_tokenize)
'''
