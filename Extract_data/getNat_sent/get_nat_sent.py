'''
Created on Aug 3, 2016

@author: Janice & Andrew
'''

from __future__ import print_function
import os
from lxml import etree as ET
import re
from collections import defaultdict
import nltk
import nltk.data
#from nltk.tokenize import sent_tokenize

def getSent(term, text):
    result = []
    tokenizedText = sent_detector.tokenize(text)
    for __ in tokenizedText:
        if term in __:
            result.append(__)
        else:
            continue

    return(result)

def parseNat(tree):
    #    code_to_nationality, countrySet, codeSet = decode_nat()
    pat = re.compile(r"(<.*>)|({{citation.*)")
    #nat_symbol = set()
    subpat = re.compile(r"\(.*?\)|\-|\|")
    #etree declaration
    root = ET.Element('root')
    
    for event, page in tree:
        nat = set()
        #doctext declaration
        docText = page.text
        #outputList declaration
        outputList = []
        if page.find('info') != None:
            info = page.find('info')
            for line in info.text.split("\n"):
                if u"nationality" in line.lower():
                    line = re.sub(pat, '', line)
                    l = line.split("=")
                    if l[0].strip() != u"nationality":
                        continue
                    l = filter(None, l)
                    if len(l) > 2:
                        print("len>2:", l)
                    elif len(l) == 2:
                        for n in l:
                            if len(n.strip()) == 0:
                                break
                            nationality = [n.strip() for n in l[1].split("/")]
                            for nation in nationality:
                                if "[[" in nation or "{{" in nation:
                                    continue
                                elif re.search(subpat,nation):
                                    m = re.sub(subpat, ",", nation)
                                    for n in m.split(","):
                                        if len(n.strip())>0 and n.strip()[0].isupper():
                                            nat.add(n.strip())
                                elif "," in nation:
                                    for n in nation.split(","):
                                        if len(n.strip())>0 and n.strip()[0].isupper():
                                            nat.add(n.strip())
                                else:
                                    nat.add(nation)
                                    
                    break
                
        resultNation = ' and '.join(nat)
        if resultNation == '' or resultNation == ' ':
            resultNation = 'Unlisted'
            
            
        temp = ET.SubElement(root, 'doc', title = page.attrib['title'], nationality = resultNation)
        
        for term in nat:
            outputList = outputList + getSent(term, docText)

        
        outputText = '\n\n'.join(outputList)
        temp.text = outputText
        page.clear()
    outputTree = ET.ElementTree(root)
    return(outputTree)
    #return (nat)

def match(d, line, *args): #m1, m2, m3):
    for m in args:
        if m[0]:
            d[m[1]].append(line)
            return True
    return False



if __name__ == "__main__":
    sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
    filename = r"..\bball_finalcombined.xml"
    tree = ET.iterparse(filename,events=('end',), tag='doc')
    result = parseNat(tree)
    result.write("Nat_Sents_Simple.xml", encoding = 'utf-8', pretty_print = True)
    #symbol_dict = extract_symbol_phrase(list(nat_symbol))
    #for a,b in symbol_dict.items():
        #print(a)
        #for i in b[-3:]:
            #print("  {}".format(i))

"""
c
  {{USA}}
  {{TUR}}
  {{CAN}}
flagC_CN
  {{flagicon|Estonia}} [[Estonia]]n
  {{flagicon|Israel}} [[Israel]]i
  {{flagicon|EST}} [[Estonia]]n
flagC
  {{flag|Canada}}
  {{flag|Philippines}}
  {{flag|Germany}}
CN
  [[Iceland]]ic
  [[Estonia]]n
  [[Uruguay]]an
N_Cc
  American {{flagicon|USA}}
C_CN
  [[Republic of the Congo|Congo]]lese
  [[FYR Macedonia|Macedonia]]n
flagCN
  {{flag|Lithuania}}n
C
  [[Tunisia]]
  [[United Arab Emirates]]
  [[Qatar]]
C_N
  [[Bulgaria|Bulgarian]]
  [[France|French]]
  [[Azerbaijanis|Azerbaijani]]
flagC_C
  {{flagicon|Philippines}} [[Philippines]]
flagc_C_cn
  {{flagicon|CAN}} [[Canada|Canadian]]
  {{flagicon|Greece}} [[Greece|Greek]]
  {{flagicon|United States}} [[United States|American]]
flagCc_N
  {{flagicon|KOR}}Korean
  {{flagicon|USA}} American
  {{flagicon|PHI}} Filipino
"""
