'''
Created on Aug 4, 2016

@author: Janice
'''
import re
from lxml import etree as ET


def get_infobox_vals(tree):
    #    code_to_nationality, countrySet, codeSet = decode_nat()
    pat = re.compile(r"(<.*>)|({{citation.*)")
    nat = set()
    nat_symbol = set()
    subpat = re.compile(r"\(.*?\)|\-|\|")
    for event, page in tree:
        if page.find('info') != None:
            info = page.find('info')
            for line in info.text.split("\n"):
                if u"nationality" in line.lower():
                    line = re.sub(pat, '', line)
                    l = line.split("=")
                    if l[0].strip() != u"nationality":
                        continue
                    l = filter(None, l)
                    if len(l) > 2:
                        print("len>2:", l)
                    elif len(l) == 2:
                        for n in l:
                            if len(n.strip()) == 0:
                                break
                            nationality = [n.strip() for n in l[1].split("/")]
                            for nation in nationality:
                                if "[[" in nation or "{{" in nation:
                                    nat_symbol.add(nation)
                                elif re.search(subpat,nation):
                                    m = re.sub(subpat, ",", nation)
                                    for n in m.split(","):
                                        if len(n.strip())>0 and n.strip()[0].isupper():
                                            nat.add(n.strip())
                                elif "," in nation:
                                    for n in nation.split(","):
                                        if len(n.strip())>0 and n.strip()[0].isupper():
                                            nat.add(n.strip())
                                else:
                                    nat.add(nation)
                    break
        page.clear()
    return (nat,nat_symbol)