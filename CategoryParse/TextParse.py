'''
@author Andrew
Parse of Wiki Index to match Categories and pull infobox
'''
#import xml.etree.cElementTree as etree
#from xml.dom import minidom
from lxml import etree
import os.path
import codecs
import re

#Replaces opening and closing brackets and | before Info box entries
def stripInfoBox(line):
    strng = line.replace("| ", "")
    return strng

#build tree from category and turn infobox features into XML tags
def parseCategory(files, criteria):
    tree = etree.iterparse('../filename.xml', tag = 'doc')
    #root = tree.getroot()


    root1 = etree.Element('root')

#loop through children nodes of input XML and match text, if match pull infobox
    for event, element in etree.iterparse(files, tag ='doc'):

        temp = etree.SubElement(root1, 'doc', id = element.attrib['id'], title = element.attrib['title'])
                    #create info tag and populate text
        try:
            docs = etree.SubElement(temp, "Info", topic = element[1].attrib['topic'])
            workingText = element[1].text.strip().split('\n')
            for line in workingText:
                            #tempData = re.sub(r"(?<=\=)\s+", "p", line)
                tagData = line.split(" = ")
                            #tagData[0] = tagData[0].strip()
                if len(tagData) != 2:
                    continue
                else:
                                #check for empty values
                    if tagData[1] == "":
                        continue
                    elif tagData[1] == " ":
                        continue
                    else:
                                    #create child nodes based on infobox
                        temporary = etree.SubElement(docs, tagData[0].strip()).text = tagData[1]
        except:
            pass

        del element


#todo: iterparse while iterparsing input document to search for a specific tag and extract text
#build and write tree to xml output
    endTree = etree.ElementTree(root1)
    return endTree

if __name__ == "__main__":

    outputTree = parseCategory('../enwiki_info_cat_iter23.xml', 'history')
    #for child in
    outputTree.write("FullParse.xml", encoding = 'utf-8', pretty_print = True)
