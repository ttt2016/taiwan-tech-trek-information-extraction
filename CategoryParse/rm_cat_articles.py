'''
Created on Jul 20, 2016

@author: Janice

Removes all category articles in the 12GB enwiki-20160601-pages-articles-text.xml file since those aren't helpful to us.
'''

from lxml import etree as ET
import os.path
import codecs
import re


if __name__ == "__main__":
    article_file = "../enwiki-text_manual.xml"
    #article_file = "../enwiki-text_manual_small.xml"
    tree = ET.parse(article_file)
    root = tree.getroot()
    for page in root.findall('doc'):
        doc_title = page.attrib['title']
        catpat = re.compile(r"Category:")
        if re.match(catpat, doc_title):
            root.remove(page)
    tree.write("../enwiki-text_manual_nocat2.xml", encoding='utf-8', pretty_print = True)
    print("END")

    #removal does not work in iterparse
