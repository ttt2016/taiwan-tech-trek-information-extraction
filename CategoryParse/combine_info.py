'''
Created on Jul 19, 2016

@author: Janice
Started with Andrew's TextParse and made changes.
Parse of Wiki Index to match Categories and pull infobox
'''
from __future__ import print_function
#import xml.etree.cElementTree as etree
#from xml.dom import minidom
from lxml import etree as ET
import os.path
import codecs
import re





def get_article(new_root, page, text_root):
    doc_id = page.attrib['id']
    for article in text_root:
        if doc_id == article.attrib['id']:
            doc = ET.SubElement(new_root, "doc", id = doc_id, title = page.attrib['title'])
            
            cat_child = page.find('category')
            cat = ET.SubElement(doc, "category")
            cat.text = cat_child.text 


            info_child = page.find('info')
            if info_child != None:
                info = ET.SubElement(doc, "info", topic = info_child.attrib['topic'])
                info.text = info_child.text
                
            doc.text = article.text
            text_root.remove(article) 
            break

    
if __name__ == "__main__":
    info_cat_file = "../enwiki-articles_parsed_small2.xml"
    article_file = "../enwiki-text_manual_small_nocat2.xml"
    
    new_root = ET.Element("root")
    
    info_cat_tree = ET.iterparse(info_cat_file, tag='doc')
    text_tree = ET.ElementTree(file=article_file)
    text_root = text_tree.getroot()
    
    for event, page in info_cat_tree:
        get_article(new_root, page, text_root)
        page.clear()
    
    new_tree = ET.ElementTree(new_root)
    new_tree.write("enwiki-combinedSmall.xml", encoding = 'utf-8', pretty_print = True)
    print("END")
