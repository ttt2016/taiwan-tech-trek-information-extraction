
'''
@author Andrew
rudimentary parse of expanded Wiki index
'''
import xml.etree.cElementTree as etree
#import lxml
import os.path
import codecs

#Replaces opening and closing brackets and | before Info box entries
def stripInfoBox(line):
    strng = line.replace("| ", "")
    strng = strng[:-3]
    return strng



if __name__ == "__main__":

    treez = etree.ElementTree(file = '../wiki_info_cat_5000.xml')
    root = treez.getroot()

    #root1 = etree.ElementTree(rootTest)
    root1 = etree.Element('root')

    for child in root:

        if 'history' in child[0].text.lower():
            temp = etree.SubElement(root1, 'doc', id = child.attrib['id'], title = child.attrib['title'])

            newString = stripInfoBox(child[1].text)
            doc = etree.SubElement(temp, "Info", topic = child[1].attrib['topic'])
            doc.text = newString

    endTree = etree.ElementTree(root1)
    endTree.write("test.xml", encoding = "utf-8")
