'''
Created on Jul 6, 2016

@author: Janice
'''
from __future__ import print_function

import xml.etree.cElementTree as ET
import nltk
import os
import re 

def get_birthday(text):
    #print("Get birthday", text)
    
    #Longest month (spelling) - September - 9 chars

    #Most successfull but does not do born in 1917 Beloretsk, Bachkortostan - 2010 in USA
    #pat = re.compile(r'(\w{2,12} \d{1,2}, \d{4}|\d{1,2} \w{2,12} \d{4}|\d{4})((\s{1,4}.{1,2}\s{1,4})(\w{2,12} \d{1,2}, \d{4}|\d{1,2} \w{2,12} \d{4}|\d{4}))?\)?')
    x = 0
    for m in re.finditer(r'\(.*?\)', text):
        match_day = re.findall(r'(\w{1,12} \d{1,2}, \d{4}|\d{1,2} \w{1,12} \d{4}|\d{4})', m.group(0))
        if match_day:
            return match_day
        if x > 2:
            return
        x += 1
        
    '''
    paren_pat = re.compile(r'\(.*?\)')
    match = paren_pat.search(text)
    if match:
            match_day = re.findall(r'(\w{1,12} \d{1,2}, \d{4}|\d{1,2} \w{1,12} \d{4}|\d{4})',match.group(0))
            print(match_day)
    '''
    #pat = re.compile(r'(\w{1,12} \d{1,2}, \d{4}|\d{1,2} \w{1,12} \d{4}|\d{4})(.{0,5}(\w{1,12} \d{1,2}, \d{4}|\d{1,2} \w{1,12} \d{4}|\d{4})?\)?')
    #pat = re.compile(r'[\w]+[\d]{1,2},[\d]{4}|([\d]{1,2}[\w]+[\d]{4}|[\d]{4})((.{0,5})(([\d]{1,2}[\w]+[\d]{4})|([\w]+[\d]{1,2},[\d]{4})|([\d]{4})))?\)?')
    # m = re.match(r'(\w+)@(\w+)\.(\w+)','username@hackerrank.com')

    
if __name__ == "__main__":
    old_tree = ET.ElementTree(file = '../basketball_parsed_short.xml')
    old_root = old_tree.getroot()
    
    newfile = open("basketball_birthdays.txt", 'w')
    
    for child in old_root:
        #sent1 = nltk.sent_tokenize(child.text)
        dates = get_birthday(child.text)
        if not dates:
            newfile.write("{:10}\t| {:30}\t| {}\n".format(child.attrib['id'], child.attrib['title'], "No dates found."))
        elif len(dates) == 1:
            newfile.write("{:10}\t| {:30}\t| {:20}\n".format(child.attrib['id'], child.attrib['title'], dates[0]))
        elif len(dates) > 1:
            newfile.write("{:10}\t| {:30}\t| {:20}\t| {:20}\n".format(child.attrib['id'], child.attrib['title'], dates[0], dates[1]))
    print("Program Complete")

if __name__ != "__main__":
    f = open("basketball_birthdays.txt", 'w')
    f.write('hi')
    f.close()
    print("end")
