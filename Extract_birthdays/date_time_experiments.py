'''
Created on Jul 7, 2016

@author: Janice
'''


examples = """
(28 January 1921  - 29 January 2014)
(born in 1922)
(born Hilal Tuba Tosun in 1970)
(born December 31, 1986)
(Greek: born March 29, 1990)
(pronounced "bee-line"; born February 5, 1953)
(1919 to 18 July 1994)
(though his name is Americanized as Zen Puzinauskas; born March 4, 1920; died July 16, 1995)
(born in 1917, Beloretsk, Bashkortostan - 2010 in USA)
(born 11 May 1962 in Sofia)
(September 9, 1907 - November 9, 1985)
(1913 - 1981)
(born in 1916)
(Americanized his name as Connie Savickas; October 14, 1908, Punsk - 1992, Chicago)
(born c. 1939)
"""
import re
examples = examples.split("\n")
print examples
for i in range(len(examples)):
    #examples[i] = re.sub(r"\(|\)", "", examples[i])
    examples[i] = examples[i].strip(' ()')
    print examples[i]

    