'''
Created on Aug 10, 2016

@author: Janice
'''
from __future__ import print_function
from nltk.tokenize import StanfordTokenizer
import codecs
from lxml import etree as ET

from nltk.tokenize import StanfordTokenizer
import nltk
from collections import defaultdict
import time

def match(sentences, patValMatch, patValCorrect, patList, nats, dbSet):
    for a,b,i in patList:
        for sent in sentences:           
            x = sent.find(a)
            y = sent.find(b)
            if x == -1 or y == -1:
                continue #no match (try later sentences)
            word = sent[x+len(a):y].strip()
            if word.startswith("n "): # to equate 'a' and 'an'
                word = word[2:].strip() 
            if word not in dbSet:
                continue #no match (try later sentences) 
            if x != -1 and y != -1: #some match
                patValMatch[(a,b,i)] += 1    
            if word in nats:
                patValCorrect[(a,b,i)] += 1
            break  #matched something, whether correct or not

def calc(numCorrect, numCaptured, total_articles):
    #returns precision & recall
    return (numCorrect/float(numCaptured), numCorrect/float(total_articles))

def capture(patList, tree, word_tokenize, sent_detector, dbSet):
    patValMatch = defaultdict(int) #pattern: matched something
    patValCorrect = defaultdict(int) #pattern: correct match
    total_articles = 0
    start = time.time()
    for event, page in tree:
        if total_articles % 50 == 0:
            print(time.time()-start)
        nats = [y.strip() for y in page.attrib['nationality'].split('and')]
        sentences = sent_detector.tokenize(page.text)[:5]
        try:
            sentences[0] = sentences[0].split("\n")[3]
        except:
            continue
        total_articles += 1
        match(sentences, patValMatch, patValCorrect, patList, nats, dbSet)
    patStats = dict()
    for k,v in patValMatch.items():
        patStats[k] = calc(patValCorrect[k], v, total_articles)
    return (patStats, total_articles)

if __name__ == "__main__":
    
    dbFile = open("InfoNat_DB_2.txt", "r")
    dbSet = set(i.strip() for i in dbFile.readlines())
    print(sorted(dbSet)[:10])
    
    sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
    word_tokenize = StanfordTokenizer('../stanford-postagger/stanford-postagger.jar')#, encoding='utf-8')
    treefile = r"Testing_rand2.xml"
    patfile = codecs.open('patternsFinaltokThurs.txt', 'r', 'utf-8')
    patfile.readline()
    patfile.readline()    
    line = patfile.readline()
    pattern_list = []
    while line:
        pattern_list.append(line.split("\t\t"))
        line = patfile.readline()
         
     
    tree = ET.iterparse(treefile,events=('end',), tag='doc')
    resultfile = codecs.open("extract_indivscore.txt", "w", "utf-8")
    patStats, numArticles = capture(pattern_list, tree, word_tokenize, sent_detector, dbSet)
    resultfile.write(u"num articles: {}\n".format(numArticles))
    for k,v in sorted(patStats.items(), key = lambda x: (-x[1][0], x[1][1])):
        resultfile.write(u"precision: {:5.3}  recall: {:5.3}  pattern: {:20}/{:20} ({:4})  \n".format(v[0], v[1], k[0], k[1], k[2][:-1]))        
 
#     for k,v in sorted(patStats.items(), key = lambda x: (x[1][0], x[1][1]), reverse=True):
#         resultfile.write(u"precision: {:5.2}  recall: {:5.2}  pattern: {:20}/{:20} ({:4})  \n".format(v[0], v[1], k[0], k[1], k[2][:-1]))        
    resultfile.close()
    print("Complete")