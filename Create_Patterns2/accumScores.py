'''
Created on Aug 11, 2016

@author: Janice
'''
from __future__ import print_function
from lxml import etree as ET
import nltk
import codecs
import re
import time

#training

def match(sentences, a, b, natDB):
    someMatch = False
    correctMatch = False
    for sent in sentences:
        x = sent.find(a)
        y = sent.find(b)
        if x == -1 or y == -1:
            continue #no match (try later sentences)       
        word = sent[x+len(a):y].strip()
        if word.startswith("n "): # to equate 'a' and 'an'
            word = word[2:].strip() 
        if word not in dbSet:
            continue #no match (try later sentences) 
        if x != -1 and y != -1: #some match
            someMatch = True  
        if word in natDB:
            correctMatch = True
        break  #matched something, whether correct or not         
    return someMatch, correctMatch

def accum(tree, patA, patB, wf, accumCorrect, accumMatched, sent_detector, numArticles, natDB):
    foundSoFar = 0
    correctSoFar = 0
    for page in tree:
        sentences = sent_detector.tokenize(page.text)[:5]
        nats = [y.strip() for y in page.attrib['nationality'].split('and')]
        try:
            sentences[0] = sentences[0].split("\n")[3]
        except:
            continue
        someMatch, correctMatch = match(sentences, patA, patB, natDB)
        if someMatch:
            foundSoFar += 1
            tree.remove(page)
        if correctMatch:
            correctSoFar += 1
    accumCorrect += correctSoFar
    accumMatched += foundSoFar
    try:
        wf.write(u"pat: {:20} /{:20}\n".format(patA,patB).encode('utf8', 'ignore'))
        if foundSoFar != 0:
            wf.write(u"ind prec: {:>4}/{:<4}={:<5.3}\tind rec: {:>4}/{:<4}={:<5.3}\n".format(
                    correctSoFar, foundSoFar, correctSoFar/float(foundSoFar),
                    correctSoFar, numArticles, correctSoFar/float(numArticles)))
        else:
            wf.write(u"ind prec: {:>4}/{:<4}={:<5}\tind rec: {:>4}/{:<4}={:<5.3}\n".format(
                    correctSoFar, foundSoFar, 0,
                    correctSoFar, numArticles, correctSoFar/float(numArticles)))        
        wf.write(u"cum prec: {:>4}/{:<4}={:<5.3}\tcum rec: {:>4}/{:<4}={:<5.3}\n\n".format(
                accumCorrect, accumMatched, accumCorrect/float(accumMatched),
                accumCorrect, numArticles, accumCorrect/float(numArticles)))
    except:
        pass
    return (accumCorrect, accumMatched)

if __name__ == "__main__":
    patternFile = r"extract_indivscore.txt"
    trainingFile = r"Training_rand2.xml"
    wf = open("accumTrainScore.txt", "w")
    start = time.time()
    
    dbFile = open("InfoNat_DB_2.txt", "r")
    dbSet = set(i.strip() for i in dbFile.readlines())    
    
    sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
    removableTree = ET.ElementTree(file=trainingFile)

    removableT = removableTree.getroot()
    
    patFile = codecs.open(patternFile, 'r', 'utf-8')
    numArticlesLine = patFile.readline() #num articles: 2259
    numArticles = int(numArticlesLine.split()[-1].strip())
    print("----", numArticles)
    line = patFile.readline()
    getPatB = re.compile(r"\(([\d\s]+)?\)")
    correctSoFar = 0
    foundSoFar = 0
    articleSet = set()
    
    patNum = 0
    while line:
        patNum += 1
        print(time.time()-start)
        fullPat = line[line.find("pattern: ")+9:]
        patA, patB = [a.strip() for a in fullPat.split("/")]
        patB = re.sub(getPatB, '', patB).strip()
        correctSoFar, foundSoFar = accum(removableT, patA, patB, wf, correctSoFar, foundSoFar, sent_detector, numArticles, dbSet)
        line = patFile.readline()
        
    print("Complete")
        
    patFile.close()
    wf.close()
    