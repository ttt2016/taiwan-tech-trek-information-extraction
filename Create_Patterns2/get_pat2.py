'''
Created on Aug 10, 2016

@author: Janice
'''

from __future__ import print_function
import codecs
from lxml import etree as ET

from nltk.tag.stanford import StanfordNERTagger
from nltk.tag.stanford import StanfordPOSTagger

from nltk.tokenize import word_tokenize
from nltk.tokenize import StanfordTokenizer
from collections import defaultdict
from nltk.corpus import stopwords
# import os
import nltk
import re
import time

def get_index(nat, tok):
    for n in nat:
        if n not in tok:
            return -1
    if len(nat) == 1:
        try:
            x = tok.index(nat[0])
        except:
            for idx in range(len(tok)):
                if nat[0] in tok[idx]:
                    x = idx
                    break 
        return x
    else:
        start = 0  
        #  For 2+ word nationalities -- find where it actually appears.
        #  For those times when first word appears but second one does not follow.
        #  "Mario Morales Micheo (born November 13, 1957), a.k.a. "Quijote" Morales for his 
        #  ability to conquer both scoring and team championships in **Puerto Rico's** BSN 
        #  league, is a **Puerto Rican** basketball player.    
        while start < len(tok):
            try:
                x = tok.index(nat[0], start)
            except:
                for idx in range(len(tok)):
                    if nat[0] in tok[idx]:
                        x = idx
                        break
            if " ".join(nat) == " ".join(tok[x:x+len(nat)]):
                break
            else:
                start = x+1 
        return x

def fill(tok, patLen3_3, nat, idx):
    tokA = tok[max(0, idx-3):idx] 
    try:
        if tokA[-1] == u'an':
            tokA[-1] = u'a' 
         
    except:
        pass
    tokB = tok[idx+len(nat):idx+len(nat)+3] 
    try:
        if tokB[-1] == u'an':
            tokB[-1] = u'a'
        elif tokB[-1].endswith(',') or tokB[-1].endswith('.'):
            tokB[-1] = tokB[-1][:-1]
    except:
        pass
    phraseA = " ".join(tokA)
    phraseA = re.sub(r"-RRB-", u")", phraseA)
    phraseA = re.sub(r"-LRB-", u"(", phraseA)
    phraseB = " ".join(tokB)
    phraseB = re.sub(r"-RRB-", u")", phraseB)
    phraseB = re.sub(r"-LRB-", u"(", phraseB)
    patLen3_3["\t\t".join([phraseA, phraseB])]+=1

def process_sent(sent, nats, word_tokenize, patLen3_3):
    tok = word_tokenize.tokenize(sent)
    for nat in nats:
        if nat not in sent:
            continue
        nat = nat.split()
        if len(nat) == 0:
            continue      
        x = get_index(nat, tok)
        if x == -1:
            continue
        fill(tok, patLen3_3, nat, x)

def accumPat(tree, word_tokenize, writefile):
    print("AccumPat")
    patLen3_3 = defaultdict(int)
    v = 0
    start = time.time()
    for event, page in tree:
        v+=1
        if v%50 == 0:
            print(time.time()-start)
#         if u"Larry Ayuso" in page.attrib['title']:
#             print(u"{}".format(page.attrib['title']).encode('utf8', 'ignore'))
        sentenceText = page.find('Sentences')
        if sentenceText == None or sentenceText.text == None:
            print('sentenceText == None, ', page.attrib['title'].encode('utf8', 'ignore'))
            page.clear()
            continue
        try:
            nats = [y.strip() for y in page.attrib['nationality'].split('and')]
            sentence = sent_detector.tokenize(sentenceText.text)[0]
            try:
                sentence = sentence.split("\n")[3]
            except:
                pass
        except:
            print(u"Error: {}".format(page.attrib['title']).encode('utf8','ignore'))
            print(u"{}".format(sentenceText.text).encode('utf8','ignore'))
            continue
#         if u"Larry Ayuso" in page.attrib['title']:
#             print("HEREA", sentence)
        process_sent(sentence, nats, word_tokenize, patLen3_3)
    return patLen3_3

if __name__ == "__main__":
    filename = r"Training_rand2.xml"
    
    sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
    st = StanfordNERTagger('../stanford-ner/classifiers/english.all.3class.distsim.crf.ser.gz',
                       '../stanford-ner/stanford-ner.jar')#, encoding = 'utf-8')
    sp = StanfordPOSTagger('../stanford-postagger/models/english-bidirectional-distsim.tagger',
                           '../stanford-postagger/stanford-postagger.jar')#, encoding = 'utf-8')
    word_tokenize = StanfordTokenizer('../stanford-postagger/stanford-postagger.jar')#, encoding='utf-8')
    
    tree = ET.iterparse(filename,events=('end',), tag='doc')
    
    stop_words = set(stopwords.words('english'))
    

    f = codecs.open('patternsFinal2.txt', 'w', 'utf-8')
    pattern_dict = accumPat(tree, word_tokenize, f)
    total = sum(pattern_dict.values())
    f.write(u'total articles: {}\n'.format(total))
    f.write(u'total patterns: {}\n'.format(len(pattern_dict.keys())))
    for k,v in sorted(pattern_dict.items(), key = lambda x: x[1]):
        f.write(u"{}\t\t{}\n".format(k,v))
    f.close()
    print("Complete")

#     dict_collection = accumPat(tree, st, sp, stop_words, word_tokenize, sent_detector)
# 
#     names = ['pd_pre5', 'pd_post5', 'pd_pre4', 'pd_post4', 'pd_pre3', 'pd_post3', 
#              'pd_preAll5', 'pd_postAll5', 'pd_preAll4', 'pd_postAll4', 'pd_preAll3', 'pd_postAll3',
#              'pd_full2', 'pd_fullAll2', 'pd_full3', 'pd_fullAll3',
#              'rm_pre5', 'rm_post5', 'rm_pre4', 'rm_post4', 'rm_pre3', 'rm_post3',
#              'rm_preAll5', 'rm_postAll5', 'rm_preAll4', 'rm_postAll4', 'rm_preAll3', 'rm_postAll3',
#              'rm_full2', 'rm_fullAll2', 'rm_full3', 'rm_fullAll3']
#     
#     for i in range(len(names)):
#         filename = names[i] + ".txt"
#         with open(filename, 'w') as f:
#             write_dict_to_file(f, dict_collection[i])  
#     print("Complete")  