from lxml import etree as ET
import os.path
import codecs
import re
#import regex

DOCUMENT = "../bball_finalcombined.xml"
OUTPUT_FILE = "../bballplayers.txt"
file = codecs.open(OUTPUT_FILE,"w", "utf-8")

for event, element in ET.iterparse(DOCUMENT, tag = 'doc'):
    if "basketball player" in element.find("category").text:
        if element.find("info") is not None:
            if ('basketball' or 'NBA') in element.find("info").attrib['topic']:
                retired = 0;
                file.write(element.attrib['title'] + ":\n")
                print(element.attrib['title'] + "\n")
                info_text = element.find('info').text

                #Writing the height
                height = re.search(r'height_ft\s*=\s*(\d+)', info_text)
                heightstring = ""
                if height:
                    height_in = re.search(r'height_in\s*=.*(\d+)((\s)|(<))', info_text)
                    if height_in:
                        height_in_string = re.search(r'(\d+\.)?\d+(?=((\s)|(<)))', height_in.group(0)).group(0)
                    else:
                        height_in_string = "0"
                    heightstring = re.search(r'\d+', height.group(0)).group(0) + '\'' + height_in_string + '\"'
                else:
                    height = re.search(r'height_m\s*=.*(\d+)(?=(\n)|(}))', info_text)
                    if height:
                        heightstring = re.search(r'(\d+\.)?\d+', height.group(0)).group(0) + "m"
                    else:
                        height = re.search(r'height_cm\s*=\s*(\d+)', info_text)
                        if height:
                            heightstring = re.search(r'\d+', height.group(0)).group(0) + 'cm'
                        else:
                            height = re.search(r'height\s*=.*', info_text)
                            if height:
                                height = re.search(r'(?<==)[\|\w\d {}=\.\[\]()]+', height.group(0))

                                if height:
                                    value = re.search(r'(\d+\.)?\d+', height.group(0))
                                    unit = re.search(r'((cm)|m|(ft)|(in))', height.group(0))
                                    if value and unit:
                                        heightstring = value.group(0) + unit.group(0)                   

                file.write(heightstring + "\n")
                
                #Writing the weight
                weight = re.search(r'weight_lbs?\s*=\s*(\d+)', info_text)
                weightstring = ""
                if not weight:
                    weight = re.search(r'weight_kg\s*=\s*(\d+)', info_text)
                    if weight:
                        weightstring = re.search(r'\d+', weight.group(0)).group(0) + "kg"
                    else:
                        weight = re.search(r'weight\s*=(.+)', info_text)
                        if weight:
                            weight = re.search(r'(?<==)[\|\w\d {}=\.\[\]()]+', weight.group(0))
                            if weight:
                                value = re.search(r'(\d+\.)?\d+', weight.group(0))
                                unit = re.search(r'((lb)|(kg))', weight.group(0))
                                if value and unit:
                                    weightstring = value.group(0) + unit.group(0)
                else:
                    weightstring = re.search(r'\d+', weight.group(0)).group(0) + "lb"
                file.write(weightstring + "\n")
                #Write the nationality
                nationality = re.search(r'nationality\s*=[ \t]*(.+)(\n|<r)', info_text)
                if nationality:
                    nationality = re.search(r'(?<==)[ \t]*.+(?=(\n)|(<r))', nationality.group(0))
                    nationality = re.sub(r'(\[\])|(<.*>)', '', nationality.group(0))
                    file.write(nationality.strip() + '\n')
                #Determine retired or active
                end = re.search(r'career_end\s*=\s*(\d+)', info_text)
                start = re.search(r'career_start\s*=\s*(\d+)', info_text)
                if end:
                    end = re.search(r'\d+', end.group(0))
                    retired = 1
                    if start:
                        file.write("Played from " + re.search(r'\d+', start.group(0)).group(0) +" - "+ end.group(0) + "\n")
                    else:
                        file.write("Played until " + end.group(0) + "\n")
                else:
                    if start:
                        file.write("Playing from " + re.search(r'\d+', start.group(0)).group(0) + " - present\n")
                        #Write team if active
                        team = re.search(r'(?<!_)team\s*=[ \t]*(.+)\n', info_text)
                        if team:
                            team = re.search(r'(?<==)[ \t]*.+(?=\n)', team.group(0))
                            if team.group(0).strip():
                                file.write("Plays for the " + team.group(0).strip(' []') + '\n')

                #Write the career position
                if retired == 1:
                    position = re.search(r'career_position\s*=[ \t]*.+', info_text)
                    if not position:
                       position = re.search(r'position\s*[ \t]*.+', info_text)
                else:
                    position = re.search(r'position\s*=[ \t]*.+', info_text)
                    
                if position:
                    position = re.search(r'(?<==)[ \t]*.+', position.group(0))
                    if position:
                        position = re.sub(r'(?<=(/|\[))[\w ()]+?\|', '', position.group(0))
                        position = re.sub(r'[{\[\]}]', '', position)
                        position = re.sub(r'<ref>.+</ref>', '', position)
                        position = re.sub(r'<.*?>', '', position)

                        if position.strip():
                            file.write(position.strip() + "\n")

                #birth day and death date if included
                birthdaystring = re.search(r'birth_date\s*=(.+)\n', info_text)
                if birthdaystring:
                    byear = re.search(r'\d{4}', birthdaystring.group(0))
                    bmonth = re.search(r'(?<=\|)\d{1,2}(?=\|)', birthdaystring.group(0))
                    bday = re.search(r'(?<=\|)\d{1,2}(?=(})|(\|[a-z])|(\|})|(\|\d+=))', birthdaystring.group(0))
                    if not bmonth:
                        bmonth = re.search(r'[A-Za-z]+', birthdaystring.group(0))
                        bday = re.search(r'\d{1,2}', birthdaystring.group(0))
                    if byear:
                        if bmonth and bday:
                            file.write("Born on " + bmonth.group(0) + "/" + bday.group(0) + "/" + byear.group(0) + "\n")
                        else:
                            file.write("Born on " + byear.group(0) + "\n")
                deathstring = re.search(r'death_date\s*=[ \t]*(.+)\n', info_text)
                if deathstring:
                    #print(deathstring.group(0))
                    dyear = re.search(r'\d{4}', deathstring.group(0))
                    dmonthday = re.findall(r'(?<=\|)\d{1,2}(?=\|)', deathstring.group(0))
                    if dyear: 
                        if len(dmonthday) >= 2:
                        #print(dyear.group(0))
                            file.write("Died on " + dmonthday[0] + "/" + dmonthday[1] + "/" + dyear.group(0) + "\n")
                        else:
                            file.write("Died in " + dyear.group(0) + "\n")

                #ending
                file.write("---------------------------------\n\n")
                


