'''
Created on Jul 20, 2016

@author: Janice
'''


from __future__ import print_function
import os
from lxml import etree as ET
import re


#"film" in cat_child.text and "adapted into films" not in cat_child.text:
def filter(text):
    #round1
    if "film" not in cat_child.text:
        return False
    #round2
    if "adapted into films" in cat_child.text:
        return False
    #round3
    if ""

if __name__ == "__main__":
    
    #filename = "../enwiki-articles_parsed.xml"
    filename = "enwiki_films.xml"
    new_root = ET.Element("root")
    categories = set()
    word = "film"
    
    tree = ET.iterparse(filename, tag='doc')
    #tree = ET.iterparse(filename,events=('end',))
    pat = re.compile(r"([A-Za-z0-9 ]+)")
    #x=0
    
    cat_file = open("film_categories3.txt","w")
    rm_file = open("film_removed3.txt", "w")
    
    for event, page in tree:
        cat_child = page.find('category')
        # if "film" in cat_child.txt >> "enwiki_films.xml"
        # if "film" in cat_child.text and 
        #    "adapted into films" not in cat_child.text:
        #    >> "enwiki_films2.xml"
        if filter(cat_child.text): 
            for category in pat.findall(cat_child.text):
                if word in category:
                    cat_file.write("{}\n".format(category))
                    
            doc = ET.SubElement(new_root, "doc", id = page.attrib['id'], title = page.attrib['title'])
            
            cat = ET.SubElement(doc, "category")
            cat.text = cat_child.text 
            
            info_child = page.find('info')
            if info_child != None:
                info = ET.SubElement(doc, "info", topic = info_child.attrib['topic'])
                info.text = info_child.text
        else:
            rm_file.write("{}\n".format(page.attrib['title']))
        page.clear()
                
#         info = page.find('info')
#         if info != None:
#             if word in info.attrib["topic"]:
#                 topics.add(info.attrib["topic"].strip())
        #x += 1
        #if x > 10000:

    new_tree = ET.ElementTree(new_root)
    new_tree.write("enwiki_films2.xml", encoding = 'utf-8', pretty_print = True)

    cat_file.close()
    rm_file.close()
    print("Complete")
        