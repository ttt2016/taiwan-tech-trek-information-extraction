'''
Created on Jul 5, 2016

@author: Janice

Get Articles with "basketball player".
'''
from __future__ import print_function
import os
import xml.etree.cElementTree as ET
# import codecs
# import re

 
# def get_tags(text):
#     id = (re.search(r'id=\"(\d+)\"', text)).group(1)
#     url = (re.search(r'url=\"(.*?)\"', text)).group(1)
#     title = (re.search(r'title=\"(.*?)\"', text)).group(1)
#     return (id, url, title)

if __name__ == "__main__":
    old_tree = ET.parse(os.path.dirname(__file__)+ '/../wiki_med_short_processed.xml')
    old_root = old_tree.getroot()

    root = ET.Element("root")
    
    for child in old_root:
        if "basketball" in child.text:
            doc = ET.SubElement(root, "doc")
            doc = child
            print(doc.text)
    tree = ET.ElementTree(root)
    tree.write("bball_med_short_filtered1.xml", encoding = "utf-8")
    print("Program Complete")
    