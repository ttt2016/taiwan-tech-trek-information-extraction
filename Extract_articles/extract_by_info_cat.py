'''
Created on Jul 20, 2016

@author: Janice
'''

from __future__ import print_function
import os
from lxml import etree as ET
import re

if __name__ == "__main__":
    
    filename = "../enwiki-articles_parsed.xml"

    topics = set()
    categories = set()
    word = "park"
    tree = ET.iterparse(filename, tag='doc')
    #tree = ET.iterparse(filename,events=('end',))
    pat = re.compile(r"([A-Za-z0-9 ]+)")
    #x=0
    for event, page in tree:
        cat = page.find('category')
        for category in pat.findall(cat.text):
            if word in category:
                categories.add(category)
        info = page.find('info')
        if info != None:
            if word in info.attrib["topic"]:
                topics.add(info.attrib["topic"].strip())
        #x += 1
        #if x > 10000:
        if len(topics) > 15:
            break
    
    with open(word+"_cat.xml", "w") as text_file:
        for i in sorted(categories):
            text_file.write("{}\n".format(i))
    with open(word+"_topc.xml", "w") as text_file:
        for i in sorted(topics):
            text_file.write("{}\n".format(i))
    print("Complete")
        