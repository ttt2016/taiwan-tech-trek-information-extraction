'''
Created on Jul 12, 2016

@author: Janice

Read from Wikipedia articles data dump.
Get ID, Title, Infobox, and Category.
If there are multiple infoboxes, just get the first
one because the first one is probably more/most "important".
"important" == famous for that topic.

Format:
<root>
    <doc id="1495" title="Australian Labor Party">
        <category>
        Australian labour movement
        Labour parties
        Political parties established in 1891
        Social democratic parties
        Australian Labor Party| 
        1891 establishments in Australia
        Progressive Alliance
        Former member parties of the Socialist International
        </category>
        <info topic="Political party">
        name = Australian Labor Party
        logo = [[File:Logo.laborbanner.png|230px]]
        leader = [[Bill Shorten]]
        </info>
    </doc>
    <doc id="1496" title="August 18">
        <category>
        Days of the year
        August
        </category>
    </doc>
</root>

'''


from __future__ import print_function

from lxml import etree as ET
import nltk
import os
import re 
import time #delestart_time = time.time()te later

if __name__ == "__main__":
    #start_time = time.time()
    tree = ET.ElementTree(file="../../../ma/enwiki-20160601-pages-articles-multistream.xml" )
    root = tree.getroot()
    new_root = ET.Element("root")
    prev_doc_id = -1;
    x = 0;
    for page in root:
        #Disregard 'redirect' XML blocks
        if page.find('redirect') != None:
            continue
        
        #Get Title and ID
        try:
            doc_title = page.find('title').text
            doc_id = page.find('id').text
            
            #Get Page Text
            rev = page.find('revision')
            page_text = rev.find('text').text
            
            #Disregard if no category
            x = page_text.find("[[Category:")
            if x == -1:
                continue
            
            #Create Doc & Category
            doc = ET.SubElement(new_root, "doc", id = doc_id, title = doc_title)
            cat = ET.SubElement(doc, "category")
            
            #Format Category
            cat_text = re.sub(r"\[\[Category:|\]\]", '',  page_text[x:], flags=re.I)
            cat.text = "\n" + cat_text + "\n"

            #Get InfoBox
            pat = re.compile(r"(\{\{Infobox(.*?)\n\}\})", re.DOTALL | re.I)
            m = pat.search(page_text)
            
            #Create and format Infobox
            if m != None:
                full_info = m.group(0)
                i = full_info.find("\n")
                info_text = re.sub(r"(?<!.)\| ?", '', full_info[i+1:-3], flags=re.I)
                
                info_topic = re.sub(r"\{\{infobox ", '', full_info[:i], flags=re.I)
                info_box = ET.SubElement(doc, "info", topic = info_topic)
                info_box.text = "\n" + info_text + "\n"  
            if doc_id != None:
                prev_doc_id = doc_id
        except:
            print("Error: (prev article--{})".format(prev_doc_id))
            x+=1;
            if (x >= 10):
                pass
            else:
                break
        
    #Finalize Tree and Write it into XML file
    new_tree = ET.ElementTree(new_root)
    new_tree.write("../wiki_info_cat_5000.xml", encoding='utf-8', pretty_print = True)
    
    print("End")
    #print("-- {} seconds --".format(time.time() - start_time))

    
