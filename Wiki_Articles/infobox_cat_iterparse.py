'''
Created on Jul 12, 2016
TESTING WITH ERRORS
@author: Janice

Read from Wikipedia articles data dump.
Get ID, Title, Infobox, and Category.
If there are multiple infoboxes, just get the first
one because the first one is probably more/most "important".
"important" == famous for that topic.

Outputs two XML files.
The first one are the regular articles with the following format.
Output Format:
<root>
    <doc id="1495" title="Australian Labor Party">
        <category>
        Australian labour movement
        Labour parties
        Political parties established in 1891
        Social democratic parties
        Australian Labor Party| 
        1891 establishments in Australia
        Progressive Alliance
        Former member parties of the Socialist International
        </category>
        <info topic="Political party">
        name = Australian Labor Party
        logo = [[File:Logo.laborbanner.png|230px]]
        leader = [[Bill Shorten]]
        </info>
    </doc>
    <doc id="1496" title="August 18">
        <category>
        Days of the year
        August
        </category>
    </doc>
</root>
The second XML file are all the Wikipedia pages of Categories.


Janice: lead programmer - design, code, debug, test, revise, code.
Andrew: collaborator in design; introduced the element tree iterparse tool.
Kevin: collaborated on regex patterns for extracting infobox and category.
Took some time experimenting with encoding. Revised XML_breaker for UTF-8 encoding (Kevin).
Saved file in various formats and manipulated mediawiki [wrapper]-tag's attributes (Janice).
'''


from __future__ import print_function

from lxml import etree as ET
import nltk
import os
import re 
import time #delestart_time = time.time()te later

def construct_cat(page, doc_id, doc_title, cat_root):
    rev = page.find('revision')
    page_text = rev.find('text').text
    if page_text == None:
        return
    x = page_text.find("[[Category:")
    if x == -1:
        return

    #Create Doc & Category    
    cat_doc = ET.SubElement(cat_root, "doc", id = doc_id, title = doc_title)
    cat = ET.SubElement(cat_doc, "category")
    
    #Format Category
    cat_text = re.sub(r"\[\[Category:|\]\]", '',  page_text[x:], flags=re.I)
    cat.text = "\n" + cat_text + "\n"
    
    return

if __name__ == "__main__":
    #file_name = "../wiki_articles_5000.xml"
    file_name = "../enwiki-articles.xml"

    tree = ET.iterparse(file_name,events=('end',), tag='page')
    new_root = ET.Element("root")
    cat_root = ET.Element("root")

#     counter = 0
#     counter2 = 1
    
    for event, page in tree: 
        #Disregard 'redirect' XML blocks
        if page.find('redirect') != None:
            page.clear()
            continue
        
        #Get Title and ID
        doc_id = page.find('id').text
        doc_title = page.find('title').text
        
        #Write "Category articles" elsewhere
        catpat = re.compile(r"Category:")
        if re.match(catpat, doc_title):
            construct_cat(page, doc_id, doc_title, cat_root)
            page.clear()
            continue            

        #Get Page Text
        rev = page.find('revision')
        page_text = rev.find('text').text

        #Wk4 Errors came up saying articles had no page_text
        #Wikipedia:WikiProject Missing encyclopedic articles/biographies/G
        #Wikipedia:WikiProject Missing encyclopedic articles/biographies/H
        #Wikipedia:Articles for deletion/Brett McElvaine
        if page_text == None:
            page.clear()
            continue
        
        
        #Disregard if no category
        x = page_text.find("[[Category:")
        if x == -1:
            continue
        
        #Create Doc & Category
        doc = ET.SubElement(new_root, "doc", id = doc_id, title = doc_title)
        cat = ET.SubElement(doc, "category")
        

        #Format Category
        y = page_text[x:].find("\n\n")
        if y != -1:
            end_text = page_text[x:x+y]
        else:
            end_text = page_text[x:]

        cat_text = re.sub(r"\[\[Category:|\]\]", '',  end_text, flags=re.I)
        cat.text = "\n" + cat_text + "\n"

        #Get InfoBox
        #pat = re.compile(r"(\{\{Infobox(.*?)\n\}\})", re.DOTALL | re.I)
        pat = re.compile(r"(\{\{Infobox(.*?)\n\}\})", re.DOTALL | re.I)
        m = pat.search(page_text)
        
        #Create and format Infobox
        if m != None:
            full_info = m.group(0)
            pat2 = re.compile(r"\{\{Infobox(.*?)}}", re.I)
            m2 = pat2.search(full_info)
            if m2 == None:
                i = full_info.find("\n")
                info_text = re.sub(r"(?<!.)\| ?", '', full_info[i+1:-3], flags=re.I)
                info_topic = re.sub(r"\{\{infobox ", '', full_info[:i], flags=re.I)
                z = info_topic.find("<!")
                if z != -1:
                    info_topic = info_topic[:z].strip()
                z = info_topic.find("|")
                if z != -1:
                    info_topic = info_topic[:z].strip()
                info_box = ET.SubElement(doc, "info", topic = info_topic)
                info_box.text = "\n" + info_text + "\n"      
            #Else
                # No content in infobox
        
#         counter += 1
#         if counter == 80000:
#             print("still running {}".format(counter2))
#             counter += 1
#             counter = 0
        page.clear()
        
        
        #Finalize Tree and Write it into XML file
    new_tree = ET.ElementTree(new_root)
    new_tree.write(file_name[:-4] +"_parsed.xml", encoding='utf-8', pretty_print = True)
    #new_tree.write("../wiki_info_cat_iter.xml", encoding='utf-8', pretty_print = True)
    new_cat_tree = ET.ElementTree(cat_root)
    new_cat_tree.write(file_name[:-4] + "_cats.xml", encoding='utf-8', pretty_print = True)
    print("End")
    
    
if __name__ != "__main__":
    pass
    
