from __future__ import print_function
import xml
import xml.etree.cElementTree as ET
import codecs
import re


def get_tags(text):
    id = text[text.find("id\"")+10:text.find("\" url=\"")]
    url = text[text.find("\" url=\"")+7:text.find("\" title=\"")]
    title = text[text.find("\" title=\"")+9:text.find("\">")]
    #print (id, url, title)
    id = (re.search(r'id=\"(\d+)\"', text)).group(1)
    url = (re.search(r'url=\"(.*?)\"', text)).group(1)
    title = (re.search(r'title=\"(.*?)\"', text)).group(1)

    #id = text[text.find("id\"")+10:text.find("\" url=\"")]
    #url = text[text.find("\" url=\"")+7:text.find("\" title=\"")]
    #title = text[text.find("\" title=\"")+9:text.find("\">")]
    print (id, url, title)
    return (id, url, title)

'''
text = '<doc id="1676707" url="https://en.wikipedia.org/wiki?curid=1676707" title="Flight service station">'
Flight service station
</doc>
'''
if __name__ != "__main__":
    #inputFile = raw_input("Enter filename: ")
    inputFile = r"enwiki-20160601-text.xml"
    root = ET.Element("root")
    f = codecs.open(inputFile, encoding = 'utf-8')
    tags = dict(id = "", url = "", title = "")
    text = "\n"
    for line in f:
        temp_skip = False
        #print (line, end = "")
        if temp_skip and line[0:5].lower() != '<doc':
            temp_skip = False
        elif line[0:4].lower() == '<div':
            #don't write stuff
            temp_skip = True
        elif line[0:5].lower() == '</div':
            #don't write stuff
            pass
        elif line[0:4].lower() == '<doc':
            i, u, t = get_tags(line)
            tags['id'] = i
            tags['url'] = u
            tags['title'] = t
        elif line[0:5].lower() == '</doc':
            doc = ET.SubElement(root, "doc", id = tags['id'], url = tags['url'], title = tags['title']).text = text
            text = "\n"
        else:
            #write
            text += line.replace("&", "&amp")
    tree = ET.ElementTree(root)
    tree.write("filename.xml", encoding = "utf-8")
#

# with open('wiki_med_short.xml', 'r') as f:
#     temp_skip = False
#     for line in f:
#         print (line, end = "")
#         if temp_skip and line[0:5].lower() != '</doc':
#             temp_skip = False
#         elif line[0:4].lower() == '<div':
#             #don't write stuff
#             temp_skip = True
#         elif line[0:5].lower() == '</div':
#             #don't write stuff
#             pass
#         else:
#             #write
#             print (line.replace("&", "&amp"), end = "")
#             
