'''
Created on Jul 1, 2016

@author: Janice
'''
#from lxml import etree
import xml.etree.cElementTree as ET
from xml.etree.cElementTree import ParseError
from lxml import etree

#attribute keys: url, id, title
if __name__ == "__main__":
    tree = ET.ElementTree(file='wiki_med_short.xml')

    root = tree.getroot()
    print (root.tag, root.attrib)
    article_ids = {}

    for child in root:
        print child.attrib
        if "basketball" in child.text:
            article_ids.add(child['id'])    
    print article_ids
    
