'''
Created on Jul 1, 2016

@author: Janice
        Andrew
used to get articles with "basketball player" in them
'''
import xml.etree.cElementTree as ET
from xml.etree.cElementTree import ParseError
from lxml import etree
import os.path
import codecs

#attribute keys: url, id, title
if __name__ == "__main__":
    #f = codecs.open('../processed_med_short.xml', encoding = 'utf-8')
    tree = ET.ElementTree(file='../filenamemanual.xml')

    root = tree.getroot()
    #print (root.tag, root.attrib)
    article_ids = {}

    root1 = ET.Element("root")

    for child in root:
        #print child.attrib
        if "basketball player" in child.text:
            doc = ET.SubElement(root1, 'doc', id = child.attrib['id'], title = child.attrib['title'])
            doc.text = child.text
            text = '\n'
    endTree = ET.ElementTree(root1)
    endTree.write("basketball_parsed.xml", encoding = "utf-8")
