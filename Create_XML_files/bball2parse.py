'''
@author Kevin
'''

import xml.etree.cElementTree as ET
from xml.etree.cElementTree import ParseError
from lxml import etree
import os.path
import codecs

if __name__ == "__main__":
    tree = ET.ElementTree(file ='../basketball_parsed.xml')
    root = tree.getroot()
    root1 = ET.Element("root")
    bad = ["Category:", "Year award", "Shaq Fu", "film", "NBA Draft", "List of", "Miss New York", "Portal:", "Basketball Coaches Association", "(comics)", "Euroleague All-Final", "(author)", "(disambiguation)", "FIBA Hall of Fame", "is an alternative rock", "of the Year", "is a regional" ]
    for child in root:
        check = child.text[:300]
        if "basketball player" in check and not any(phrase in check for phrase in bad): 
            doc = ET.SubElement(root1, 'doc', id = child.attrib['id'], title = child.attrib['title'])
            doc.text = child.text
            
    endTree = ET.ElementTree(root1)
    endTree.write("../basketball_parsed2.xml", encoding = "utf-8")
