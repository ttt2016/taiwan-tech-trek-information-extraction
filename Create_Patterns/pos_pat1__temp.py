'''
Created on Aug 5, 2016

@author: Janice
'''

from __future__ import print_function
from lxml import etree as ET

from nltk.tag.stanford import StanfordNERTagger
from nltk.tag.stanford import StanfordPOSTagger

from nltk.tokenize import word_tokenize
from nltk.tokenize import StanfordTokenizer
from collections import defaultdict
from nltk import  ne_chunk
# import os
import nltk
import sys

def getPat(tree):
    x = 0
    for event, page in tree:
        if page.attrib['nationality'] == "Unlisted":
            page.clear()
            continue
        x +=1
        if x == 10:
            break
        sent = sent_detector.tokenize(page.text)
        sent[0] = sent[0].split("\n")[3]
        try:
            print("1", sent[0])
            print("2", sent[1])
            print("3", sent[2])
        except:
            print(sys.exc_info())
        page.clear()
#         pageText = page.text
        


if __name__ == "__main__":
    filename = r"Nat_Sents_Simple.xml"

    sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
    st = StanfordNERTagger('../stanford-ner/classifiers/english.all.3class.distsim.crf.ser.gz',
                       '../stanford-ner/stanford-ner.jar')#, encoding = 'utf-8')
    sp = StanfordPOSTagger('../stanford-postagger/models/english-bidirectional-distsim.tagger',
                           '../stanford-postagger/stanford-postagger.jar')#, encoding = 'utf-8')
    word_tokenize = StanfordTokenizer('../stanford-postagger/stanford-postagger.jar')#, encoding='utf-8')

    tree = ET.iterparse(filename,events=('end',), tag='doc')
    
    result = getPat(tree)
    

