'''
Created on Aug 3, 2016

@author: Janice & Andrew
'''

from __future__ import print_function
import os
from lxml import etree as ET
import re
from collections import defaultdict
from collections import OrderedDict
import nltk
import nltk.data
import random
import codecs
#from nltk.tokenize import sent_tokenize



"""----PATTERNS----"""
#c - code        N - nationality
#C - Country     CN - [country_to_]nationality
#xy (either)
#x_y (separate)
#    {{flagicon|INA}} Indonesian
patflagCc_N = re.compile(r"^\{\{flagicon\|(.+)?\}\}[ ]?([A-Z][A-Za-z/-]+)$")
#        American {{flagicon|USA}}
patN_Cc = re.compile(r"^([A-Z][A-Za-z/-]+)[ ]?\{\{flagicon\|(.+?)\}\}$")
#    [[Icelanders|Icelandic]]
patC_N = re.compile(r'^\[\[([A-Za-z\- ]+?)\|([A-Za-z\- ]+?)\]\]$')

#    [[Croatia]]n
patCN = re.compile(r"^\[\[([A-Za-z \-]*?\]\](?:[a-z]+))$")
#    {{flag|Lithuania}}n
patflagCN = re.compile(r'^(?:\{\{flag\|([[A-Za-z \-]*?\}\}(?:[a-z]+)))$')
#    {{flagicon|Jordan}} [[Jordan]]ian
patflagC_CN = re.compile(r'^\{\{flagicon\|([A-Za-z\- ]+?)\}\}[ ]{0,3}\[\[([A-Z][A-Za-z\- ]+\]\][a-z]+)$')

#    {{FRA}}-{{BEN}}
patc = re.compile(r'(\{\{[A-Z]{2,3}\}\}){1,3}')
#    [[Taiwan]]
patC = re.compile(r'^\[\[([A-Za-z\- ]*?)\]\]$')
#    {{flag|Canada}}
patflagC = re.compile(r'^\{\{flag\|([A-Za-z -]+?)\}\}$')
#    {{flagicon|Philippines}} [[Philippines]]
patflagC_C = re.compile(r'(\{\{flagicon\|([A-Za-z\- ]+?)\}\})[ ]{0,3}(\[\[[A-Z][A-Za-z\- ]+\]\])$')
#    {{flagicon|USA}} [[United States|USA]]
#patflagc_C_cn = re.compile(r'^\{\{flagicon\|([A-Za-z\- ]+?)\}\}[ ]{0,3}\[\[([A-Za-z\- ]*?(\([A-Za-z\- ]+\))?\|([A-Za-z\- ]*?))\]\]$')
patflagc_C_cn = re.compile(r'^\{\{flagicon\|([A-Za-z\- ]+?)\}\} {0,3}\[\[([A-Za-z\- ]*?)\|([A-Za-z\- ]*?)\]\]$')

patfinder = re.compile(r"(\[\[[A-Za-z' \-]+\]\](?![a-z]))|(\[\[[A-Za-z' \-]+\]\][a-z]+)|(\[\[[A-Za-z' \-\(\)]+\|[A-Za-z' \-]+\]\](?![a-z]))|(\[\[[A-Za-z' \-]+\|[A-Za-z' \-]+\]\][a-z]+)")
#A: [[Czechoslovak]] (patC)
#B: [[Jordan]]ian (patCN)
#C: [[French people|French]] (patC_N)
#D: [[Republic of the Congo|Congo]]lese (patC_CN)
patC_CN = re.compile(r"^\[\[([A-Za-z' \-]+)\|([A-Za-z' \-]+\]\][a-z]+)$")
patFind2 = re.compile(r"\[\[([A-Za-z \-\(\)\']+)\|([A-Za-z \-\(\)\']+)\]\]")
"""----PATTERNS----"""

def loadDicts(fileDB):
    d = {}
    with open(fileDB) as f:
        lines = f.readlines()

        for line in lines:
            (key,val) = line.strip('\n').split('\t')
            d[key] = val

    print ('...Successfully Loaded DB...')
    return (d)



def getSent(term, text):
    result = []
    tokenizedText = sent_detector.tokenize(text)
    for __ in tokenizedText:
        if term in __:
            result.append(__)
        else:
            continue

    return(result)

def extractNat(i):
    #    {{flagicon|INA}} Indonesian
    m1 = re.match(patflagCc_N,i)
    #    American {{flagicon|USA}}
    m2 = re.match(patN_Cc,i)

    #    {{flag|Lithuania}}n
    m5 = re.match(patflagCN, i)

    #    {{flagicon|Estonia}} [[Estonia]]n
    m6 = re.match(patflagC_CN,i)

    #    {{flagicon|USA}} [[United States|USA]]
    m11 = re.match(patflagc_C_cn, i)

    #    {{flag|Canada}}
    m9 = re.match(patflagC, i)

    #    {{flagicon|Philippines}} [[Philippines]]
    m10 = re.match(patflagC_C, i)

    #    [[Icelanders|Icelandic]]
    m3 = re.match(patC_N,i)

    #    [[Croatia]]n
    m4 = re.match(patCN,i)

    #    [[Taiwan]]
    m8 = re.match(patC,i)

    #    {{FRA}}-{{BEN}}
    m7list = re.findall(patc, i)

    #C, CN, C_N, C_CN
    m_findlist = re.findall(patfinder, i)

    if m1:
        #return m1.group()
        #print ('1')
        #print (m1.group(2))
        return([m1.group(2)])
    elif m2:
        #return m1.group()
        #print ('2')
        #print (m2.group(1))
        return([m2.group(1)])
    elif m5:
        #return m1.group()
        #print ('5')
        #print ([''.join(m5.group(1).split('}}'))])
        return ([''.join(m5.group(1).split('}}'))])
    elif m6:
        #return m1.group()
        #print ('6')
        #print (m6.group(2))
        return([''.join(m6.group(2).split(']]'))])
    elif m11:
        #return m1.group()
        #print ('11')
        if len(m11.group(1)) == 3 and len(m11.group(3)) == 3:
            print ('process this country code')
            nat = codeNat[m11.group(1)]
        else:
            nat = m11.group(3)

        #print (nat)
        return([nat])
    elif m9:
        #country
        #return m1.group()
        #print ('9')
        #print (m9.group(1))
        try:
            nat = [countryNat[m9.group(1)]]
        except:
            nat = 'NA'
            print ("...Not in Database...")
        #print (nat)
        return(nat)
    elif m10:
        #return m1.group()
        #country
        #print ('10')
        try:
            nat = countryNat[m10.group(2)]
        except:
            nat = 'NA'
            print ("...Not in Database...")
        #print (nat)
        return([nat])
    elif m3:
        #return m1.group()
        #print ('3')
        #print (m3.group(2))
        return([m3.group(2)])
    elif m4:
        #return m1.group()
        #print ('4')
        #print(''.join(m4.group(1).split(']]')))
        return([''.join(m4.group(1).split(']]'))])
    elif m8:
        #return m1.group()
        #country multiple
        #print ('8')
        try:
            nat = countryNat[m8.group(1)]
        except:
            nat = 'NA'
            print ("...Not in Database...")
        #print (nat)
        return ([nat])
    elif m7list:
        #print ('m7list')
        result = []
        for code in m7list:

            try:
                nat = codeNat[code.strip('{{').strip('}}')]
                result.append(nat)
            except:
                print ("...Not in Database...")
        #print (result)
        return (result)
    elif m_findlist:
        if len(m_findlist) > 0:
            results = []
            for find in m_findlist:
                if find[0]:
                    try:
                        nat = countryNat[find[0].group(2)]
                    except:
                        nat = 'NA'
                        print ("...Not in Database...")
                    if nat != 'NA':

                        results.append(nat)
                elif find[1]:
                    
                    results.append((''.join(find[1].strip('[[').split(']]'))))
                elif find[2]:
                    l = re.sub(r"\(.+?\)|people", '', find[2])
                    m = re.findall(patFind2,l)
                    for x in m:
                        if x != '':
                            results.append(x[1])
                    #print (results)
                elif find[3]:
                    m = re.search(patC_CN, find[3])
                    results.append(''.join(m.group(2).split(']]')))
                    #print (results)

        #print (m_findlist)
        return (results)
    else:
        return ('')




def parseNat(tree, codeNat, countryNat, resultFile):
    #    code_to_nationality, countrySet, codeSet = decode_nat()
    dbSet = set()
    pat = re.compile(r"(<.*>)|({{citation.*)")
    #nat_symbol = set()
    subpat = re.compile(r"\(.*?\)|\-|\|")
    #etree declaration
    root = ET.Element('root')
    root1 = ET.Element('root')
    counter = 0
    for event, page in tree:
        nat = set()
        natSymb = set()
        #doctext declaration
        docText = page.text
        #outputList declaration
        outputList = []
        if page.find('info') != None:
            info = page.find('info')
            for line in info.text.split("\n"):
                if u"nationality" in line.lower():
                    line = re.sub(pat, '', line)
                    l = line.split("=")
                    if l[0].strip() != u"nationality":
                        continue
                    l = filter(None, l)
                    if len(l) > 2:
                        print("len>2:", l)
                    elif len(l) == 2:
                        for n in l:
                            if len(n.strip()) == 0:
                                break
                            nationality = [n.strip() for n in l[1].split("/")]
                            for nation in nationality:
                                if "[[" in nation or "{{" in nation:
                                    #print (nationality)
                                    #return list of nationalities
                                    for returns in (extractNat(nation)):
                                        #print (returns)
                                        if returns != 'NA':
                                            nat.add(returns)
                                elif re.search(subpat,nation):
                                    m = re.sub(subpat, ",", nation)
                                    for n in m.split(","):
                                        if len(n.strip())>0 and n.strip()[0].isupper():
                                            nat.add(n.strip())
                                elif "," in nation:
                                    for n in nation.split(","):
                                        if len(n.strip())>0 and n.strip()[0].isupper():
                                            nat.add(n.strip())
                                else:
                                    nat.add(nation)
                            break

        #Search for unique sentences containing Nationality
        for term in nat:
            for sent in getSent(term, docText):
                if sent not in outputList:
                    outputList.append(sent)
                else:
                    continue
        if len(outputList) > 0:
            #Output string of every sentence separated by two new lines
            outputText = '\n\n'.join(outputList)


        for __ in nat:
            dbSet.add(__)
        #String creation for XML Tree Nationality attribute
        resultNation = ' and '.join(nat)

        if resultNation == '' or resultNation == ' ':
            resultNation = 'Unlisted'
        #Node creation (subelement)
        randomness = random.random()
        if resultNation != 'Unlisted':

            if randomness < 0.75:
                if counter <= 6000:
                    counter += 1
                    temp = ET.SubElement(root, 'doc', OrderedDict([('title', page.attrib['title']), ('nationality', resultNation)]))
                    temp.text = page.text
                    sent = ET.SubElement(temp, 'Sentences')
                    sent.text = outputText
                else:
                    temp1 = ET.SubElement(root1, 'doc', OrderedDict([('title', page.attrib['title']), ('nationality', resultNation)]))
                    temp1.text = page.text
                    sent1 = ET.SubElement(temp1, 'Sentences')
                    sent1.text = outputText



            else:
                temp1 = ET.SubElement(root1, 'doc', OrderedDict([('title', page.attrib['title']), ('nationality', resultNation)]))
                temp1.text = page.text
                sent1 = ET.SubElement(temp1, 'Sentences')
                sent1.text = outputText


        #Set subelement text as the resulting string of sentences
        '''
        if counter <= 2000:
            sent = ET.SubElement(temp, 'Sentences')
            sent.text = outputText
        else:
            sent1 = ET.SubElement(temp1, 'Sentences')
            sent1.text = outputText
        '''
        page.clear()
    for word in dbSet:
        if word != '' or word != ' ':
            resultFile.write(word + '\n')
    outputTree = ET.ElementTree(root)
    output1Tree = ET.ElementTree(root1)
    print (counter)
    return(outputTree, output1Tree)
    #return (nat)

def match(d, line, *args): #m1, m2, m3):
    for m in args:
        if m[0]:
            d[m[1]].append(line)
            return True
    return False



if __name__ == "__main__":
    sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
    filename = r"..\..\bball_finalcombined.xml"
    codeNat = loadDicts('rel_code_nat.txt')
    countryNat = loadDicts('rel_country_nat.txt')
    tree = ET.iterparse(filename,events=('end',), tag='doc')
    resultFile = codecs.open("InfoNat_DB_2.txt", "w", "utf-8")
    training, result = parseNat(tree, codeNat, countryNat, resultFile)
    training.write("Training_rand2.xml", encoding = 'utf-8', pretty_print = True)
    result.write("Testing_rand2.xml", encoding = 'utf-8', pretty_print = True)
    resultFile.close()


"""
c
  {{USA}}
  {{TUR}}
  {{CAN}}
flagC_CN
  {{flagicon|Estonia}} [[Estonia]]n
  {{flagicon|Israel}} [[Israel]]i
  {{flagicon|EST}} [[Estonia]]n
flagC
  {{flag|Canada}}
  {{flag|Philippines}}
  {{flag|Germany}}
CN
  [[Iceland]]ic
  [[Estonia]]n
  [[Uruguay]]an
N_Cc
  American {{flagicon|USA}}
C_CN
  [[Republic of the Congo|Congo]]lese
  [[FYR Macedonia|Macedonia]]n
flagCN
  {{flag|Lithuania}}n
C
  [[Tunisia]]
  [[United Arab Emirates]]
  [[Qatar]]
C_N
  [[Bulgaria|Bulgarian]]
  [[France|French]]
  [[Azerbaijanis|Azerbaijani]]
flagC_C
  {{flagicon|Philippines}} [[Philippines]]
flagc_C_cn
  {{flagicon|CAN}} [[Canada|Canadian]]
  {{flagicon|Greece}} [[Greece|Greek]]
  {{flagicon|United States}} [[United States|American]]
flagCc_N
  {{flagicon|KOR}}Korean
  {{flagicon|USA}} American
  {{flagicon|PHI}} Filipino
"""
